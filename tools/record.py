#!/usr/bin/python3 -B

# Copyright (C) 2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

from json import JSONDecodeError
from json import loads as json_loads
from sys import stderr

from responses import _recorder

# responses use PyYAML for its recorder
from yaml import safe_dump as yaml_safe_dump
from yaml import safe_load as yaml_safe_load

from glcollate.main import main

RECORD_FILE = "out.yaml"


def rewrite_content_type() -> None:
    # Workaround for responses not saving correct Content-Type header
    with open(RECORD_FILE) as f:
        recorded_dict = yaml_safe_load(f)

    responses_dict = recorded_dict["responses"]

    for response_dict in responses_dict:
        response = response_dict["response"]
        url = response["url"]
        content_type = response["content_type"]
        try:
            json_loads(response["body"])
        except JSONDecodeError:
            print(
                f"URL {url} not a JSON, skipping...",
                file=stderr,
            )
            continue
        else:
            print(
                f"URL {url} is a valid JSON but Content-Type is {content_type}, "
                "fixing...",
                file=stderr,
            )
            response["content_type"] = "application/json"

    with open(RECORD_FILE, mode="w") as f:
        yaml_safe_dump(recorded_dict, f, sort_keys=False)


@_recorder.record(file_path=RECORD_FILE)
def wrapper_main() -> None:
    main()


if __name__ == "__main__":
    wrapper_main()
    rewrite_content_type()
