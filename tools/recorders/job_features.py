#!/usr/bin/python3 -B

# Copyright (C) 2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2024 Collabora Ltd"

from argparse import ArgumentParser
from logging import DEBUG, basicConfig
from pathlib import Path

from responses import _recorder

from glcollate import Collate
from glcollate.job import CollateJob

RECORD_FILE = "test_collate_job_features.yaml"


@_recorder.record(file_path=RECORD_FILE)  # type: ignore [misc]
def prepare_collate_job_objects_test(main_job_id: int, secondary_job_id: int) -> None:
    """
    This method is meant to record the process to generate certain CollateJob objects, to then use it
    as input for the tests written for these functionalities.

    :return:
    """
    collate = Collate(
        gitlab_url="https://gitlab.freedesktop.org",
        namespace="virgl",
        project="virglrenderer",
    )
    main_job = collate.from_job(main_job_id)
    s = main_job.status
    secondary_job = collate.from_job(secondary_job_id)
    answer = int(
        input(
            f"\n\tPlease, retry the job {main_job.name}, and provide the job id of the new job:\n\t"
        )
    )
    main_retry = collate.from_job(answer)
    s = main_retry.status
    input(f"\n\tPlease, wake me up when the retried job finishes")
    main_retry_completed = collate.from_job(answer)
    s = main_retry_completed.status


def post_process_yaml() -> None:
    file = Path(RECORD_FILE)
    file.write_text(
        file.read_text().replace(
            "content_type: text/plain", "content_type: application/json"
        )
    )


if __name__ == "__main__":
    parser = ArgumentParser(
        description="Provide the id of two jobs in virgl/virglrenderer to record the responses "
        "to be used in the test.",
        epilog="Follow the instructions during the execution. It will require first to retry the "
        "main job (provide the id when it's on running status), and then another interaction when "
        "it finishes. It is expected all the job succeed.",
    )
    parser.add_argument(
        "--main-job",
        metavar="ID",
        type=int,
        required=True,
        help="job id of a job in virgl/virglrenderer",
    )
    parser.add_argument(
        "--secondary-job",
        metavar="ID",
        type=int,
        required=True,
        help="job id of a job in virgl/virglrenderer",
    )
    args = parser.parse_args()
    basicConfig(format="%(asctime)s - %(levelname)s - %(message)s", level=DEBUG)
    prepare_collate_job_objects_test(args.main_job, args.secondary_job)
    post_process_yaml()
