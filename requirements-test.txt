-r requirements.txt
mockgitlab @ git+https://gitlab.freedesktop.org/sergi/python-gitlab-mock@c6b69fced7b69b7047a35c9068e2d1034fa8b385
mypy==1.15.0
mypy-extensions==1.0.0
pytest==8.3.4
pytest-cov==6.0.0
responses==0.25.6
types-requests==2.32.0.20241016
