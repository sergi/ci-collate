# Python environment

## Python using virtualenv

Based on python 3.11, one can setup a virtual environments. 

```commandline
$ pip3 -V
pip 23.0.1 from /usr/lib/python3/dist-packages/pip (python 3.11)
```

A virtual environment can be created in order to use this module: 

```commandline
$ python3 -m venv ci-collate.venv
$ source ci-collate.venv/bin/activate
```

To reproduce the same virtual environment that this tools is being tested:

```commandline
$ pip install -r requirements.txt
$ pip install -e .
```

To update the packages in the requirements:

```commandline
$ pip install --upgrade pip
$ pip install gitpython python-gitlab pyzstd ruamel.yaml
$ pip list
Package            Version
------------------ ---------
certifi            2025.1.31
charset-normalizer 3.4.1
gitdb              4.0.12
GitPython          3.1.44
idna               3.10
pip                25.0.1
python-gitlab      5.6.0
pyzstd             0.16.2
requests           2.32.3
requests-toolbelt  1.0.0
ruamel.yaml        0.18.10
ruamel.yaml.clib   0.2.12
setuptools         66.1.1
smmap              5.0.2
urllib3            2.3.0
$ pip freeze --all > requirements.txt
$ pip install -e .
```

### running tests

To run the tests and lint tools, some extra dependency are required. To reproduce the environment:

```commandline
$ pip install -r requirements-test.txt
$ pip install -e .
```

But to produce the environment:

```commandline
$ pip install pytest pytest-cov responses mypy types-requests git+https://gitlab.freedesktop.org/sergi/python-gitlab-mock
$ pip freeze | grep -E "pytest|pytest-cov|responses|mypy|types-requests|python-gitlab-mock"
mockgitlab @ git+https://gitlab.freedesktop.org/sergi/python-gitlab-mock@c6b69fced7b69b7047a35c9068e2d1034fa8b385
mypy==1.15.0
mypy-extensions==1.0.0
pytest==8.3.4
pytest-cov==6.0.0
responses==0.25.6
types-requests==2.32.0.20241016
```

The `requirements-test.txt` includes the `requirements.txt` with those packages listed before.

Then, one can call pytest:

```commandline
$ ./tools/codespell.py
$ ./tools/format.py check
$ ./tools/mypy.py
$ pytest
```
