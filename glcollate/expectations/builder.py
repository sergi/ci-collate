#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from pathlib import Path

from ruamel.yaml import YAML

__all__ = ["build_expectations_paths", "is_build_stage"]


def is_build_stage(target: str, stage_name: str) -> bool:
    if target.lower() not in ["virglrenderer", "mesa", "linux"]:
        raise KeyError(f"Unmanaged {target} project")
    if target.lower() == "virglrenderer":
        if stage_name.lower() in ["build", "sanity test"]:
            return True
    if target.lower() == "mesa":
        if stage_name.lower() in [
            "sanity",
            "container",
            "git-archive",
            "build-for-tests",
            "build-only",
            "code-validation",
            "deploy",
        ]:
            return True
    if target.lower() == "linux":
        if stage_name.lower() in [
            "sanity",
            "container",
            "code-validation",
            "git-archive",
            "build",
        ]:
            return True
    return False


def build_expectations_paths(target: str) -> dict[str, dict[str, str]]:
    file_name = f"{Path(__file__).parent.absolute()}/{target}.yml"
    with open(file_name, "r") as f:
        yaml = YAML()
        return yaml.load(f)  # type: ignore [no-any-return]


# virglrenderer_expectations_paths = build_expectations_paths("virglrenderer")
# mesa_expectations_paths = build_expectations_paths("mesa")
