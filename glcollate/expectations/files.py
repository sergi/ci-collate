#!/usr/bin/env python3.11

# Copyright (C) 2023-2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023-2024 Collabora Ltd"


from collections import defaultdict
from copy import copy
from logging import getLogger
from typing import TYPE_CHECKING, List

if TYPE_CHECKING:
    from collections.abc import Iterator

logger = getLogger(__name__)


class ExpectationBlock:
    def __init__(self, block: list[str]):
        """
        This object represents a block of tests in an expectations file.
        An expectation file is a list of test names (or regex), that can include a field with the test status
        reported, and they can be a single block. But often we group tests separating with an empty line.
        Also, some lines can be comments (starting with #) to provide context.
        Then this object represents one of these blocks, with the list or tests, with perhaps a separator at
        the end (except if not the last block doesn't have it), and perhaps some comments (usually as header,
        but cannot discard in the middle of the block).
        :param block: list of lines from a expectation file
        """
        self.__raw: list[str] = []
        self.__states: dict[str, str] = defaultdict(str)
        self.__header_until: int | None = None
        header_complete: bool = False
        for i, element in enumerate(block):
            if element.startswith("#"):
                self.__raw.append(element.strip())
                if not header_complete:
                    self.__header_until = i
            elif element == "\n":
                self.__raw.append("")
                header_complete = True
            else:
                self.__insert_record(element)
                header_complete = True
        self.__n_records: int = len(self.records)
        self.__is_sorted: bool = False
        records = copy(self.records)
        records.sort()
        if records == self.records:
            self.__is_sorted = True

    @property
    def records(self) -> list[str]:
        """
        List of records (test names, perhaps with test result, perhaps a regex) in the block
        :return:
        """
        return [x for x in self.__raw if not x.startswith("#") and not len(x) == 0]

    @property
    def content_as_lines(self) -> list[str]:
        """
        Gross view of the block, to represent exactly what's in the file.
        :return:
        """
        return [
            f"{element}\n"
            if element not in self.__states
            else f"{element},{self.__states[element]}\n"
            for element in self.__raw
        ]

    def __str__(self) -> str:
        return f"{type(self).__name__}({len(self)} records)"

    def __repr__(self) -> str:
        return f"{type(self).__name__}({self.records})"

    def __len__(self) -> int:
        return len(self.__raw)

    @property
    def n_records(self) -> int:
        return self.__n_records

    @property
    def has_header(self) -> bool:
        return self.__header_until is not None

    @property
    def header(self) -> list[str] | None:
        if self.__header_until is not None:
            return self.__raw[: self.__header_until + 1]
        return None

    @property
    def has_separator(self) -> bool:
        return len(self.__raw[-1]) == 0

    def append_separator(self) -> None:
        if not self.has_separator:
            self.__raw.append("")

    def get_record_state(self, record: str) -> str | None:
        if record in self.__states:
            return self.__states[record]
        return None

    def is_block_sorted(self) -> bool:
        """
        Check if the records in the block are alphabetically sorted
        :return: true if they are sorted in the list
        """
        return self.__is_sorted

    def find_records(self, search_records: set[str]) -> tuple[set[str], set[str]]:
        """
        Given a list of records, identify what's in the block and what's not.
        :param search_records: set of records to find
        :return: set of records found and another set with the not found
        """
        records: set[str] = set(self.records)
        search_records = {record.strip() for record in search_records}
        found: set[str] = search_records.intersection(records)
        not_found: set[str] = search_records.difference(records)
        return found, not_found

    def append_records(
        self, records_to_add: set[str], test_result: str | None = None
    ) -> set[str]:
        """
        Append a set of records to the current block, but first check if they already exists to avoid
        duplications. Also, respect the empty lines at the end of the block.
        :param records_to_add: set of records
        :param test_result: optional when records have associated a test result
        :return: records added
        """
        records_to_add = {record.strip() for record in records_to_add}
        found, not_found = self.find_records(records_to_add)
        if len(not_found) > 0:
            if self.__is_sorted:
                return self.__append_sorted(not_found, test_result)
            else:
                return self.__append_at_the_end(not_found, test_result)
        return not_found

    def __append_sorted(
        self, records_to_add: set[str], test_result: str | None = None
    ) -> set[str]:
        """
         Assuming the block is alphabetically sorted, append a set of records in between the records in
         the block to maintain this sorting.
        :param records_to_add:
        :param test_result: optional when records have associated a test result
        :return:
        """
        records_to_add_sorted = list(records_to_add)
        records_to_add_sorted.sort()
        i = 0
        while i < len(self.__raw):
            line = self.__raw[i]
            if line.startswith("#"):
                i += 1
            elif len(line.strip()) == 0:
                break
            else:
                while len(records_to_add_sorted) > 0:
                    first_record_to_add = records_to_add_sorted[0]
                    pair = [records_to_add_sorted[0], line]
                    pair.sort()
                    if pair[0] != first_record_to_add:
                        i += 1
                        break
                    record = records_to_add_sorted.pop(0)
                    self.__insert_record(record, i, test_result)
                    i += 1
            if not records_to_add_sorted:
                break
        if records_to_add_sorted:
            self.__append_at_the_end(set(records_to_add_sorted), test_result)
        self.__n_records = len(self.records)
        return records_to_add

    def __append_at_the_end(
        self, records_to_add: set[str], test_result: str | None = None
    ) -> set[str]:
        """
        Insert the records to add at the end of the records stored, excluding the separator
        (empty line(s)) that could be at the end of the block.
        :param records_to_add:
        :param test_result: optional when records have associated a test result
        :return:
        """
        insert_point = len(self.__raw) - 1
        for insert_point, record in reversed(list(enumerate(self.__raw))):
            if len(record.strip()) != 0:
                insert_point += 1
                break
        records_to_add_sorted = list(records_to_add)
        records_to_add_sorted.sort()
        self.__raw = (
            self.__raw[:insert_point]
            + records_to_add_sorted
            + self.__raw[insert_point:]
        )
        if test_result:
            for record in records_to_add_sorted:
                self.__states[record] = test_result
        self.__n_records = len(self.records)
        return records_to_add

    def __insert_record(
        self,
        single_record: str,
        position: int | None = None,
        test_result: str | None = None,
    ) -> None:
        """
        Insert a record on a specific position of the raw list.
        :param single_record: record string
        :param position: position to add in the raw list
        :param test_result: state when the record doesn't include it
        :return:
        """
        record, state = self.__get_record_state(single_record)
        if position is not None:
            self.__raw.insert(position, record)
        else:
            self.__raw.append(record)
        if test_result is not None:
            self.__states[record] = test_result
        elif state is not None:
            self.__states[record] = state

    @staticmethod
    def __get_record_state(single_record: str) -> tuple[str, str | None]:
        if single_record.count(",") == 1:
            record, state = single_record.strip().split(",", 1)
            return record, state
        else:
            return single_record.strip(), None

    def remove_records(self, records_to_remove: set[str]) -> tuple[set[str], set[str]]:
        """
        Given a set of records to be remove return the records removed and the not found.
        :param records_to_remove: set of records to remove
        :return: records removed and records not found
        """
        records_to_remove = {f"{record}\n" for record in records_to_remove}
        found, not_found = self.find_records(records_to_remove)
        removed: set[str] = set()
        i = 0
        while i < len(self.__raw):
            line = self.__raw[i]
            if line.startswith("#"):
                i += 1
            elif len(line.strip()) == 0:
                break
            else:
                if line in found:
                    self.__raw.pop(i)
                    if line in self.__states:
                        self.__states.pop(line)
                    removed.add(line)
                    # Note: do not remove elements in the set "found" (the records to be removed that are
                    #  in the block), because in the rare event that there is some duplicated record,
                    #  it will remove all.
                else:
                    i += 1
        self.__n_records = len(self.records)
        return removed, not_found


class ExpectationFile:
    def __init__(self, file_name: str, lines: list[str]):
        self.__file_name: str = file_name
        self.__blocks: List[ExpectationBlock] = []
        self.__n_blocks: int = 0
        self.__n_records: int = 0
        self.__total_lines: int = 0
        self.__split_lines_in_blocks(lines)
        self.__modified: bool = False
        self.__block_for_new_records: ExpectationBlock | None = None
        self.__block_identifier: list[str] | None = None

    def __str__(self) -> str:
        return f"{type(self).__name__}(#lines={len(self)})"

    def __repr__(self) -> str:
        return f"{type(self).__name__}(#lines={len(self)},#blocks={self.n_blocks},#records={self.n_records})"

    def __len__(self) -> int:
        return self.__total_lines

    @property
    def file_name(self) -> str:
        return self.__file_name

    @property
    def blocks(self) -> List[ExpectationBlock]:
        return self.__blocks

    @property
    def is_empty(self) -> bool:
        return self.__n_blocks == 0

    @property
    def modified(self) -> bool:
        return self.__modified

    @property
    def has_new_record(self) -> bool:
        return self.__block_for_new_records is not None

    @property
    def new_records_block_identifier(self) -> list[str] | None:
        return self.__block_identifier

    @new_records_block_identifier.setter
    def new_records_block_identifier(self, block_identifier: list[str]) -> None:
        self.__block_identifier = block_identifier

    @property
    def n_blocks(self) -> int:
        return self.__n_blocks

    @property
    def n_records(self) -> int:
        return self.__n_records

    @property
    def as_lines(self) -> list[str]:
        """
        Reproduce the file content that has been split in blocks to a list of lines like a file.
        :return: list of lines for a file.
        """
        lines = []
        for block in self.__blocks:
            lines += block.content_as_lines
        return lines

    def __split_lines_in_blocks(self, lines: list[str]) -> None:
        """
        From the raw list of strings from the expectation file, build the blocks view where test records
        are grouped with empty line(s) as separator, including comment lines that can be headers or just
        records in between.
        :param lines: raw list of strings representing lines
        :return:
        """
        logger.debug(
            "Processing %d lines in %s",
            len(lines),
            self.__file_name,
        )
        blocks: list[ExpectationBlock] = []
        n_records = 0
        block_start, block_end = 0, None

        def sliding_window(_lines: list[str]) -> Iterator[tuple[int, str, str | None]]:
            for i, line in enumerate(_lines):
                next_line: str | None = _lines[i + 1] if len(_lines) > i + 1 else None
                yield i, line, next_line

        for i, line, next_line in sliding_window(lines):
            if next_line is None or (line == "\n" and next_line != "\n"):
                # its eof or next line starts a new block
                block_end = i
                new_block = ExpectationBlock(lines[block_start : block_end + 1])
                blocks.append(new_block)
                logger.debug(
                    "%d: New block of %d records between line %d and %d (%d lines)",
                    len(blocks),
                    new_block.n_records,
                    block_start + 1,
                    block_end + 1,
                    len(new_block),
                )
                block_start = block_end + 1
        self.__blocks = blocks
        self.__n_blocks = len(blocks)
        self.__n_records = sum([block.n_records for block in self.__blocks])
        self.__total_lines = sum([len(block) for block in blocks])
        logger.debug("Build %s", repr(self))

    def add_record(
        self, test_records: set[str], test_result: str | None = None
    ) -> None:
        """
        Add new records to the expectation file.
        It checks if any of the records is already in the file. If the file stores the test result, it
        also checks if the state changed, because in this case the old records has to be removed and
        add the record with the new state.
        :param test_records: set of strings with the test names
        :param test_result: test result for the test records to add
        :return:
        """
        # Check if any of these test records is already in any other block. If so remove from this insert
        test_records_to_add: set[str] = set()
        empty_blocks: set[ExpectationBlock] = set()
        for i, block in enumerate(self.__blocks):
            found, not_found = block.find_records(test_records)
            # search for records changing its state
            tests_with_different_result: dict[str, str] = {}
            for record in copy(found):
                current_record_result = block.get_record_state(record)
                if (
                    current_record_result is not None
                    and current_record_result != test_result
                ):
                    tests_with_different_result[record] = current_record_result
                    found.remove(record)
            if tests_with_different_result:
                logger.info(
                    "Records found with different status than %s: %s",
                    test_result,
                    tests_with_different_result,
                )
                block.remove_records(set(tests_with_different_result.keys()))
                if len(block.records) == 0:
                    empty_blocks.add(block)
            if found:
                logger.info(
                    "Records %s already in the %s (block %d), skip the add action",
                    found,
                    self.file_name,
                    i,
                )
            test_records_to_add |= not_found
            test_records_to_add |= set(tests_with_different_result.keys())
        for block in empty_blocks:
            self.__blocks.remove(block)
        if test_records_to_add and self.__block_for_new_records is None:
            # check if exist a block with this header, to use it as the block for additions
            # if doesn't exist, create a new one with the header (and separator at the end)
            # when add a new block at the end, make sure the previous one has a separator or
            # we will be merging to that previous block
            if self.__block_identifier is not None:
                self.__block_for_new_records = self.__search_blocks_by_header(
                    self.__block_identifier
                )
            if self.__block_for_new_records is None:
                self.__blocks[-1].append_separator()
                if self.__block_identifier is not None:
                    new_records_block = ExpectationBlock(
                        self.__block_identifier + ["\n"]
                    )
                else:
                    new_records_block = ExpectationBlock(["\n"])
                self.__blocks.append(new_records_block)
                self.__block_for_new_records = new_records_block
                self.__n_blocks += 1
        if self.__block_for_new_records is not None:
            added_records = self.__block_for_new_records.append_records(
                test_records_to_add, test_result
            )
            self.__n_records += len(added_records)
        self.__total_lines = sum([len(block) for block in self.blocks])
        self.__modified = True

    def __search_blocks_by_header(self, header: list[str]) -> ExpectationBlock | None:
        for block in self.__blocks:
            if block.has_header and header == block.header:
                return block
        return None

    def remove_records(self, test_records: set[str]) -> None:
        empty_blocks: set[ExpectationBlock] = set()
        for i, block in enumerate(self.__blocks):
            removed, not_found = block.remove_records(test_records)
            if removed:
                self.__modified = True
                logger.info(
                    "Record removal in %s: %s in block %d",
                    self.file_name,
                    removed,
                    i,
                )
                self.__n_records -= len(removed)
                if len(block.records) == 0:
                    empty_blocks.add(block)
        for block in empty_blocks:
            self.__blocks.remove(block)
        self.__total_lines = sum([len(block) for block in self.blocks])


class ExpectationsFiles:
    def __init__(self) -> None:
        self.__len = 0
        self.__fails: ExpectationFile | None = None
        self.__flakes: ExpectationFile | None = None
        self.__skips: ExpectationFile | None = None
        self.__block_identifier: list[str] | None = None

    def __iter__(self) -> Iterator[ExpectationFile]:
        if self.__fails:
            yield self.__fails
        if self.__flakes:
            yield self.__flakes
        if self.__skips:
            yield self.__skips

    def __len__(self) -> int:
        return self.__len

    @property
    def fails(self) -> ExpectationFile | None:
        return self.__fails

    @property
    def flakes(self) -> ExpectationFile | None:
        return self.__flakes

    @property
    def skips(self) -> ExpectationFile | None:
        return self.__skips

    def insert(self, name: str, new_xfile: ExpectationFile) -> None:
        if name not in ["fails", "flakes", "skips"]:
            raise KeyError(f"{name} is not a recognized expectation file")
        attrname = f"_{type(self).__name__}__{name}"
        xfile = getattr(self, attrname)
        if xfile is None:
            setattr(self, attrname, new_xfile)
            if self.__block_identifier and xfile is not None:
                xfile.new_records_block_identifier = self.__block_identifier
            self.__len += 1

    def add_block_identifier(self, block_identifier: list[str]) -> None:
        self.__block_identifier = block_identifier
        for xfile in [self.__fails, self.__flakes, self.__skips]:
            if xfile is not None:
                xfile.new_records_block_identifier = block_identifier
