#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"


from collections import defaultdict
from logging import getLogger
from re import match as re_match

from .enum import ExpectationCategories
from .files import ExpectationsFiles

logger = getLogger(__name__)


class ResultsConsistency:
    def __init__(self, test_name: str):
        """
        Counts the number of times a test had some result
        :param test_name:
        """
        self.__name = test_name
        self.__test_result_ctr: dict[ExpectationCategories, int] = defaultdict(int)

    def __str__(self) -> str:
        return f"{self.__class__.__name__}({self.__name})"

    def __repr__(self) -> str:
        return f"{self.__str__()}[{self.results_counters}]"

    @property
    def results_counters(self) -> dict[ExpectationCategories, int]:
        return dict(self.__test_result_ctr)

    @property
    def result_names(self) -> set[ExpectationCategories]:
        return set(self.__test_result_ctr.keys())

    @property
    def has_inconsistencies(self) -> bool:
        return len(self.__test_result_ctr.keys()) > 1

    def add_test_result(self, new_test_result: ExpectationCategories) -> None:
        self.__test_result_ctr[new_test_result] += 1


class ExpectationsProcessor:
    def __init__(
        self,
        name: str,
        expectations: ExpectationsFiles,
        expectation_changes: dict[ExpectationCategories, set[str]],
    ):
        self.__name = name
        self.__expectations = expectations
        logger.info(
            "For job %s current expectations: "
            "%s fail records, "
            "%s flake records, "
            "%s skip records.",
            self.__name,
            (
                self.__expectations.fails.n_records
                if self.__expectations.fails is not None
                else 0
            ),
            (
                self.__expectations.flakes.n_records
                if self.__expectations.flakes is not None
                else 0
            ),
            (
                self.__expectations.skips.n_records
                if self.__expectations.skips is not None
                else 0
            ),
        )
        self.__expectation_changes = expectation_changes
        self.__test_result_consistency: dict[str, ResultsConsistency] = {}
        self.__test_categories = list(self.__expectation_changes.keys())

    def process_expectation_changes(
        self,
        use_results_pass: bool = False,
    ) -> list[set[ExpectationCategories]] | None:
        """
        With the information about the expectation changes, process the data to
        find inconsistencies (if so, solve them) and process the results to apply
        expectation changes to the expectation files.
        :return: List of unmanaged inconsistent test results
        """
        unhandled_result_names = self.__review_result_inconsistencies()
        self.__process_pass(use_results_pass)
        self.__process_timeout()
        self.__process_flake()
        self.__process_fail_crash()
        self.__process_missing()
        self.__review_unmanaged(use_results_pass)
        return unhandled_result_names

    # first descendant level

    def __review_result_inconsistencies(
        self,
    ) -> list[set[ExpectationCategories]] | None:
        """
        The expectation changes comes with the information collated from as many
        retries the job has in a pipeline. Then, if any test finished with a different
        test results, there will be inconsistencies in this collection. The same test
        would be in more than one of the sets in the values of the expectation changes.
        :return: List of unmanaged inconsistent test results
        """
        self.__find_result_inconsistencies()
        return self.__review_expectations_with_inconsistencies()

    def __process_pass(self, use_results_pass: bool) -> None:
        test_result_types = {
            ExpectationCategories.UNEXPECTED_PASS,
            ExpectationCategories.UNEXPECTED_IMPROVEMENT_PASS,
            ExpectationCategories.UNEXPECTED_IMPROVEMENT_SKIP,
            ExpectationCategories.UNEXPECTED_IMPROVEMENT_WARN,
        }
        if use_results_pass:
            test_result_types.add(ExpectationCategories.PASS)

        expectations_fails = self.__expectations.fails
        if expectations_fails is None:
            raise RuntimeError("expectations fails were not inserted")

        for test_category in test_result_types:
            if test_category in self.__test_categories:
                logger.info(
                    "There are %s to be removed from %s",
                    test_category,
                    expectations_fails.file_name,
                )
                tests_records = self.__get_expectation_records(test_category)
                expectations_fails.remove_records(tests_records)
                self.__mark_category_as_processed(test_category)

    def __process_timeout(self) -> None:
        expectations_skips = self.__expectations.skips
        if expectations_skips is None:
            raise RuntimeError("expectations skips fails were not inserted")

        expectations_fails = self.__expectations.fails
        if expectations_fails is None:
            raise RuntimeError("expectations fails were not inserted")

        expectations_flakes = self.__expectations.flakes
        if expectations_flakes is None:
            raise RuntimeError("expectations fails were not inserted")

        if ExpectationCategories.TIMEOUT in self.__test_categories:
            logger.info(
                "There are Timeout tests to be recorded in %s. "
                "Also check if they are in %s or "
                "in %s.",
                expectations_skips.file_name,
                expectations_fails.file_name,
                expectations_flakes.file_name,
            )
            test_records = self.__get_expectation_records(ExpectationCategories.TIMEOUT)
            expectations_fails.remove_records(test_records)
            expectations_flakes.remove_records(test_records)
            expectations_skips.add_record(test_records)
            self.__mark_category_as_processed(ExpectationCategories.TIMEOUT)

    def __process_flake(self) -> None:
        expectations_skips = self.__expectations.skips
        if expectations_skips is None:
            raise RuntimeError("expectations skips fails were not inserted")

        expectations_fails = self.__expectations.fails
        if expectations_fails is None:
            raise RuntimeError("expectations fails were not inserted")

        expectations_flakes = self.__expectations.flakes
        if expectations_flakes is None:
            raise RuntimeError("expectations fails were not inserted")

        if ExpectationCategories.FLAKE in self.__test_categories:
            logger.info(
                "There are flakes to be recorded in %s. "
                "Also check if they are in %s or "
                "in %s.",
                expectations_flakes.file_name,
                expectations_fails.file_name,
                expectations_skips.file_name,
            )
            tests_records = self.__get_expectation_records(ExpectationCategories.FLAKE)
            expectations_fails.remove_records(tests_records)
            expectations_flakes.add_record(tests_records)
            # REVIEW IT: A skipped test should be run and classified as a flake
            # self.__expectations.skips.remove_records(tests_records)
            self.__mark_category_as_processed(ExpectationCategories.FLAKE)

    def __process_fail_crash(self) -> None:
        expectations_fails = self.__expectations.fails
        if expectations_fails is None:
            raise RuntimeError("expectations fails were not inserted")

        expectations_flakes = self.__expectations.flakes
        if expectations_flakes is None:
            raise RuntimeError("expectations fails were not inserted")

        test_result_types = {
            ExpectationCategories.FAIL,
            ExpectationCategories.UNEXPECTED_IMPROVEMENT_FAIL,
            ExpectationCategories.CRASH,
        }
        found_types = test_result_types.intersection(self.__test_categories)

        for test_result in found_types:
            logger.info(
                "There are %s tests to be recorded in %s. "
                "Also check if they exist with a different test result.",
                test_result,
                expectations_flakes.file_name,
            )
            tests_records = self.__get_expectation_records(test_result)
            if match := re_match(r"UnexpectedImprovement\((.*)\)", test_result):
                expectations_fails.remove_records(tests_records)
                expectations_fails.add_record(tests_records, test_result=match.group(1))
            else:
                expectations_fails.add_record(tests_records, test_result=test_result)
            self.__mark_category_as_processed(test_result)

    def __process_missing(self) -> None:
        if ExpectationCategories.MISSING in self.__test_categories:
            logger.warning(
                "There are Missing tests, but this tool don't know (yet) "
                "how to proceed with them."
            )
            self.__mark_category_as_processed(ExpectationCategories.MISSING)

    def __review_unmanaged(self, use_results_pass: bool) -> None:
        unmanaged = {
            ExpectationCategories.SKIP,
            ExpectationCategories.WARN,
            ExpectationCategories.EXPECTED_FAIL,
            ExpectationCategories.KNOWN_FLAKE,
        }
        if not use_results_pass:
            unmanaged.add(ExpectationCategories.PASS)
        for test_result in unmanaged:
            if test_result in self.__test_categories:
                logger.debug(
                    "Nothing to do with the tests in %s result",
                    test_result,
                )
                self.__mark_category_as_processed(test_result)
        if len(self.__test_categories) > 0:
            logger.warning(
                "ALERT: Unmanaged categories: %s",
                self.__test_categories,
            )

    # second descendant level

    def __find_result_inconsistencies(self) -> None:
        """
        Check the content of expectation changes for tests that could have been
        added more than once (because job retries provided different test results).
        :return:
        """
        for test_result, test_names in self.__expectation_changes.items():
            for test_name in test_names:
                if test_name not in self.__test_result_consistency:
                    self.__test_result_consistency[test_name] = ResultsConsistency(
                        test_name
                    )
                self.__test_result_consistency[test_name].add_test_result(test_result)

    def __review_expectations_with_inconsistencies(
        self,
    ) -> list[set[ExpectationCategories]] | None:
        """
        With the information about inconsistencies present in the expectation changes
        (due to tests that may report a different test result between job retries),
        review the structure to solve those inconsistencies.
        :return: List of unmanaged inconsistent test results
        """
        inconsisten_results = 0
        unhandled: list[set[ExpectationCategories]] = []
        for name, results in self.__test_result_consistency.items():
            if results.has_inconsistencies:
                inconsisten_results += 1
                logger.warning(
                    "For %s, found %s retry inconsistent results",
                    name,
                    results.results_counters,
                )
                self.__reset_inconsistent_expectations(name, results.result_names)
                if self.__inconsistency_has_a_fail(results):
                    self.__consider_inconsistency_as_flakes(name, results)
                elif self.__inconsistency_has_a_dut_issue(results):
                    self.__dut_transient_error(name, results)
                else:
                    unhandled = self.__unhandled_inconsistencies(results, unhandled)
        logger.warning(
            "Found %d tests with retry inconsistent results",
            inconsisten_results,
        )
        if unhandled:
            return unhandled
        return None

    def __get_expectation_records(self, category: ExpectationCategories) -> set[str]:
        return self.__expectation_changes[category]

    def __mark_category_as_processed(self, category: ExpectationCategories) -> None:
        self.__test_categories.pop(self.__test_categories.index(category))

    # third descendant level

    def __reset_inconsistent_expectations(
        self,
        name: str,
        results: set[ExpectationCategories],
    ) -> None:
        """
        As the test named didn't have the same result in all the job executions,
        it is necessary to remove this name from the different expectations changes.
        :param name: string with the test name
        :param results: set of results this test had
        :return: None
        """
        for result in results:
            self.__expectation_changes[result].remove(name)

    @staticmethod
    def __inconsistency_has_a_fail(results: ResultsConsistency) -> bool:
        """
        The tests should be consistent between multiple runs, but if some is not
        and have multiple results, failures or flakes prevail over the others.
        :param results: set of test results
        :return: if a result is a failure or a flake
        """
        intersection = results.result_names.intersection(
            {
                ExpectationCategories.FAIL,
                ExpectationCategories.CRASH,
                ExpectationCategories.FLAKE,
                ExpectationCategories.KNOWN_FLAKE,
            }
        )
        return len(intersection) > 0

    def __consider_inconsistency_as_flakes(
        self, name: str, results: ResultsConsistency
    ) -> None:
        """
        When a test result is not consistent between job runs (if the duplicated
        set has a failure or flake) it can be considered a flake as a final result.
        :param name: string with the test name
        :param results: set of test results
        :return: None
        """
        logger.info(
            "Test %s is considered a flake with %s results",
            name,
            results.results_counters,
        )
        self.__expectation_changes[ExpectationCategories.FLAKE].add(name)

    @staticmethod
    def __inconsistency_has_a_dut_issue(results: ResultsConsistency) -> bool:
        """
        The tests should be consistent between multiple runs, but if some is not
        and have multiple results, some results are considered a transient issue
        with the DUT.
        :param results: set of test results
        :return: if any of the results is considered a DUT transient error.
        """
        return (
            len(results.result_names.intersection({"Timeout", "Missing", "Skip"})) > 0
        )

    def __dut_transient_error(self, name: str, results: ResultsConsistency) -> None:
        """
        When a test result is not consistent between job runs (if any of those
        results could be related with a transient error with the DUT) ignore the
        transient issue.
        :param name: string with a test name
        :param results: set of test results
        :return: None
        """
        logger.info(
            "Test %s may have had DUT related issues with results %s",
            name,
            results.results_counters,
        )

    def __unhandled_inconsistencies(
        self, results: ResultsConsistency, unhandled: list[set[ExpectationCategories]]
    ) -> list[set[ExpectationCategories]]:
        """

        :param results:
        :param unhandled:
        :return:
        """
        if results.result_names not in unhandled:
            logger.warning("* Flake set %s found and not handled.", results)
            unhandled.append(results.result_names)
        return unhandled
