#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from argparse import ONE_OR_MORE, ArgumentParser, Namespace
from logging import DEBUG, ERROR, INFO, basicConfig, getLogger
from sys import stdout

from gitlab.exceptions import GitlabAuthenticationError

from .collate import Collate
from .defaults import (
    default_gitlab_token_file_name,
    default_gitlab_url,
    default_project_name,
)

logger = getLogger(__name__)


def main() -> None:
    parser = __cli_arguments()
    args = parser.parse_args()
    __setup_logging(args)
    try:
        collate = Collate(
            gitlab_url=args.gitlab_url,
            gitlab_token_file_name=args.token_file,
            namespace=args.namespace,
            project=args.project,
        )
        if args.subcommand == "job":
            job = collate.from_job(args.JOB_ID)
            if args.trace:
                stdout.write(job.trace())
            if args.artifact:
                stdout.write(str(job.get_artifact(args.artifact)))
        elif args.subcommand == "pipeline":
            pipeline = collate.from_pipeline(
                args.PIPELINE_ID, exclude_retried=args.exclude_retried_jobs
            )
            if args.artifact:
                artifacts_dict = pipeline.get_artifact(
                    args.artifact,
                    job_regex=args.job_filter,
                    status=args.status,
                    stage=args.stage,
                )
                for job_name in artifacts_dict.keys():
                    print(f"{job_name}:")
                    for job_id in artifacts_dict[job_name]:
                        print(f"\t{job_id}:")
                        artifact = artifacts_dict[job_name][job_id]
                        if artifacts_dict[job_name][job_id].has_content:
                            content = artifact.content_as_str
                            for test_result in content.splitlines():
                                print(f"\t\t{test_result}")
                        else:
                            print(f"\t\t{artifact.exception_as_str}")
        elif args.subcommand == "patch":
            pipeline = collate.from_pipeline(args.PIPELINE_ID, jobs=args.jobs)
            branch = (
                args.branch_namespace if args.branch_namespace else collate.namespace
            )
            pipeline.expectations_update(
                args.local_clone,
                branch,
                args.branch,
                args.patch_name,
                # args.with_merge_request,
                use_results_pass=args.use_results_pass,
                artifacts=args.artifacts,
                jobs=args.jobs,
                xfiles_block_header=args.header,
            )
        else:
            parser.print_help()
    except AssertionError as exception:
        logger.critical("Cannot proceed due to: %s", exception, exc_info=exception)
    except GitlabAuthenticationError as exception:
        logger.critical(
            "Review credentials: %s",
            exception,
            exc_info=exception,
        )


def __cli_arguments() -> ArgumentParser:
    """
    Define the command line behavior for the arguments and subcommands.
    :return: argument parser
    """
    parser = ArgumentParser(
        description="CLI tool to gather information about Gitlab jobs output "
        "and artifact. Individually or as members of a pipeline."
    )
    # General arguments:
    parser.add_argument(
        "--gitlab-url",
        metavar="URL",
        help=f"Url address where the gitlab server is. "
        f"Default {default_gitlab_url!r}",
    )
    parser.add_argument(
        "--namespace",
        metavar="NAME",
        help="Personal or group namespace to locate a gitlab project. If not "
        "specified it will be used the user name from the gitlab token.",
    )
    parser.add_argument(
        "--project",
        metavar="PROJECT",
        help=f"Project on the gitlab server where the information will be "
        f"lookup. Default {default_project_name!r}",
    )
    parser.add_argument(
        "--token-file",
        metavar="FILENAME",
        help=f"Path to a file containing a gitlab token. Default {default_gitlab_token_file_name!r}.",
    )
    logging_arguments = parser.add_mutually_exclusive_group()
    logging_arguments.add_argument(
        "--quiet",
        action="store_true",
        help="Reduce the logging to critical and error messages.",
    )
    logging_arguments.add_argument(
        "--debug", action="store_true", help="Increase the logging to debug messages."
    )
    # Subcommands:
    subparsers = parser.add_subparsers(
        title="subcommands",
        dest="subcommand",
        description="valid subcommands",
        help="sub-command additional help",
    )
    # collate single job
    parser_job = subparsers.add_parser("job", help="Collate from an existing job.")
    parser_job.add_argument(
        "JOB_ID",
        type=int,
        help="Identify the job to collate.",
    )
    parser_job.add_argument(
        "--trace", action="store_true", help="Print in stdout the job trace."
    )
    parser_job.add_argument(
        "--artifact", metavar="PATH", help="Print in stdout the artifact content"
    )
    # collate from a pipeline
    parser_pipeline = subparsers.add_parser(
        "pipeline", help="Collate from an existing pipeline."
    )
    parser_pipeline.add_argument(
        "PIPELINE_ID",
        type=int,
        help="Identify the pipeline to collate.",
    )
    parser_pipeline.add_argument(
        "--artifact", metavar="PATH", help="Print in stdout the artifact content"
    )
    parser_pipeline.add_argument(
        "--job-filter",
        metavar="REGEX",
        help="Only select jobs matching this regular expression",
    )
    parser_pipeline.add_argument(
        "--stage",
        metavar="STAGE",
        action="append",
        help="Only select jobs from this stage (may be specified multiple times)",
    )
    parser_pipeline.add_argument(
        "--status",
        metavar="STATUS",
        action="append",
        help="Only select jobs with a given status (may be specified multiple times)",
    )
    parser_pipeline.add_argument(
        "--exclude-retried-jobs",
        action="store_true",
        help="Exclude jobs which were retried",
    )
    # collate from a pipeline and prepare an expectations update patch
    parser_patch = subparsers.add_parser(
        "patch",
        help="Collate the failures in the jobs of a pipeline and prepare an expectations update patch.",
    )
    parser_patch.add_argument(
        "PIPELINE_ID",
        type=int,
        help="Identify the pipeline to collate.",
    )
    parser_patch.add_argument(
        "--local-clone",
        metavar="PATH",
        help="Specify the clone of the git project to use.",
    )
    parser_patch.add_argument(
        "--branch-namespace",
        metavar="NAME",
        help="Specify a different name space than namespace argument.",
    )
    parser_patch.add_argument(
        "--branch",
        metavar="BRANCH",
        help="Specify a different branch to patch than the default.",
    )
    parser_patch.add_argument(
        "--patch-name", metavar="PATCH_NAME", help="Specify the name of the patch file."
    )
    # TODO: prepare a merge request
    # parser_patch.add_argument(
    #     "--with-merge-request",
    #     action="store_true",
    #     help="Create a merge request with the expectations updated.",
    # )
    parser_patch.add_argument(
        "--use-results-pass",
        action="store_false",
        help="When processing the results.csv use the Pass results as if they "
        "are UnexpectedImprovement(Pass) in failures.csv",
    )
    parser_patch.add_argument(
        "--artifacts",
        metavar="artifacts",
        help="Artifact files to use as source of expectation changes. Complete path "
        "to files, or start with a star to delegate in the tool to search for it. "
        "eg. `--artifacts */results.csv.zst`",
        required=False,
        nargs=ONE_OR_MORE,
    )
    parser_patch.add_argument(
        "--jobs",
        metavar="jobs",
        help="Specify the subset of jobs, following regular expressions, that are "
        "meant to restrict the expectations update to only those jobs that match. "
        "eg. `--jobs .*a618.* .*a630.*`",
        required=False,
        nargs=ONE_OR_MORE,
    )
    parser_patch.add_argument(
        "--header",
        metavar="line",
        help="When update expectations use this header for the block. If exist, use the block "
        "that matches the header, elsewhere create a new block with the given header.",
        required=False,
        nargs=ONE_OR_MORE,
    )
    # Note at the end of the help
    parser.epilog = (
        f"Note: "
        f"Use GITLAB_TOKEN or {default_gitlab_token_file_name} "
        f"to provide the token with access to the project you "
        f"want to access."
    )
    return parser


def __setup_logging(args: Namespace) -> None:
    log_level = INFO
    if args.quiet:
        log_level = ERROR
    elif args.debug:
        log_level = DEBUG

    basicConfig(
        format="%(asctime)s - %(levelname)s - %(message)s",
        level=log_level,
    )


if __name__ == "__main__":
    main()
