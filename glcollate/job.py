#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from collections import defaultdict
from datetime import datetime, timedelta
from enum import IntEnum, auto
from functools import cache
from logging import getLogger
from re import match as re_match
from typing import TYPE_CHECKING

import requests
from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects import ProjectJob

from .artifact import Artifact, ArtifactsZip
from .expectations import (
    ExpectationCategories,
    ExpectationsFiles,
    ExpectationsProcessor,
    is_build_stage,
)
from .repository import Repository

if TYPE_CHECKING:
    from typing import Any, Self, TypedDict

    class TestResult(TypedDict):
        result: str
        line: int


logger = getLogger(__name__)


# it provides a way to compare status between jobs with lt & gt operators
class JobStatus(IntEnum):
    unknown = auto()
    # initialized job
    created = auto()
    scheduled = auto()
    manual = auto()
    # in progress
    pending = auto()
    running = auto()
    # completed
    skipped = auto()
    canceled = auto()
    success = auto()
    warning = auto()
    failed = auto()


def get_job_status(job: ProjectJob) -> JobStatus:
    try:
        return JobStatus[job.status]
    except KeyError:
        return JobStatus.unknown


def get_job_property(job: ProjectJob, property_name: str) -> Any:
    try:
        return getattr(job, property_name)
    except AttributeError:
        return None


def as_datetime(date_string: str | None) -> datetime:
    if isinstance(date_string, str):
        return datetime.strptime(date_string[:19] + "Z", "%Y-%m-%dT%H:%M:%S%z")
    raise TypeError(f"Cannot interpret {type(date_string)} as datetime")


class CollateJob:
    # TODO: document methods
    __expectation_files_path: str | None = None
    __expectation_files_prefix: str | None = None

    def __init__(
        self,
        gitlab_job: ProjectJob,
        project_name: str,
        exclude_retried: bool = False,
    ):
        """
        Wrapper to a gitlab job with methods to manage the access to its trace
        and the artifacts.
        :param gitlab_job: gitlab job object to wrap
        :param project_name:
        """
        self.__gl_job: dict[int, ProjectJob] = {gitlab_job.id: gitlab_job}
        self.__project_name: str = project_name
        self.__exclude_retried: bool = exclude_retried
        self.__latest_try: int = gitlab_job.id
        self.__new_latest_try: bool = False
        self.__latest_try_previous_status: str | None = None
        self.__expectation_changes: defaultdict[
            ExpectationCategories, set[str]
        ] = defaultdict(set)
        self.__duplicated_results: defaultdict[
            str, set[ExpectationCategories]
        ] = defaultdict(set)
        self.__artifacts_zip: dict[int, ArtifactsZip] = {}

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return f"CollateJob({self})"

    def __hash__(self) -> int:
        # If we define __eq__, we must define __hash__ to maintain the object hashable.
        return id(self)

    def __eq__(self, other: Any) -> bool:  # == operator
        """
        Compares if two CollateJob objects are equivalent, that means they represent the same set
        of GitLab jobs with the same status each.
        :param other:
        :return:
        """
        if not isinstance(other, CollateJob):
            return False
        if other is self:
            return True
        # Compare if the elements in both 'ids' lists are the same.
        if other.name != self.name:
            return False
        if len(set(other.ids).difference(set(self.ids))) == 0:
            return all(
                [
                    other.status_by_id(job_id) == self.status_by_id(job_id)
                    for job_id in self.ids
                ]
            )
        return True

    def __ne__(self, other: Any) -> bool:  # != operator
        """
        'Not equal' only evaluates the name of the job, when it is 'equal'
        who evaluates if their content is the same. Not-equal is much faster
        than the equality comparison.
        :param other:
        :return:
        """
        if not isinstance(other, CollateJob):
            return False
        return other.name != self.name

    def __ior__(self, other: Any) -> Self:  # |= operator
        """
        When those two objects represents the same Gitlab job and its retries, include newer information
        that could be stored in the 'other' object, so then we can consider the 'self' object refreshed.
        :param other: another CollateJob or a ProjectJob GitLab's object
        :return: CollateJob with refreshed ProjectJob members
        """
        if isinstance(other, CollateJob):
            if other != self:
                raise NameError(f"Cannot merge {other} to {self}")
            if self.__exclude_retried:
                self.__introduce_job(other[other.id])
            else:
                for job_id in other.ids:
                    self.__introduce_job(other[job_id])
        elif isinstance(other, ProjectJob):
            if other.name != self.name:
                raise NameError(f"Cannot merge {other} to {self}")
            self.__introduce_job(other)
        else:
            raise TypeError(f"Cannot merge {type(other)}")
        return self

    def __introduce_job(self, job: ProjectJob) -> None:
        """

        :param job:
        :return:
        """
        job_id = job.id
        if self.__exclude_retried:
            if job_id > self.__latest_try:
                logger.debug(
                    "Replacing in %s the latest try from %d to %d ids",
                    self,
                    self.__latest_try,
                    job_id,
                )
                self.__insert_job(job)
            elif job.id == self.__latest_try:
                internal_job = self.__gl_job[self.__latest_try]
                newer_job, older_job = self.__sort_same_id_jobs(internal_job, job)
                if id(newer_job) != id(internal_job):
                    logger.debug(
                        "Updating in %s the job with id %d",
                        self,
                        newer_job.id,
                    )
                    self.__insert_job(newer_job)
            # else should never happen, but silently ignored
        else:
            if job_id in self.ids:
                internal_job = self.__gl_job[job_id]
                newer_job, older_job = self.__sort_same_id_jobs(internal_job, job)
                if id(newer_job) != id(internal_job):
                    logger.debug(
                        "Updating in %s the job with id %d",
                        self,
                        newer_job.id,
                    )
                    self.__insert_job(newer_job)
            else:
                logger.debug(
                    "Introducing in %s the job with id %d",
                    self,
                    job.id,
                )
                self.__insert_job(job)

    def __sort_same_id_jobs(
        self,
        reference_job: ProjectJob,
        refreshed_job: ProjectJob,
    ) -> tuple[ProjectJob, ProjectJob]:
        """
        Compare two representations of a job (build in different moments in time, but with the same GitLab job id),
        to sort them with the more recent representation first.
        :param reference_job: reference to compare with the refreshed
        :param refreshed_job: more recently created object
        :return:
        """
        if reference_job.id != refreshed_job.id:
            raise AssertionError(f"Cannot sort jobs with different ids")

        def numeric_sort(i: JobStatus | float, j: JobStatus | float) -> int:
            if i > j:
                return -1
            elif i < j:
                return 1
            else:
                return 0

        def numeric_property_sort(
            property_name: str,
            is_timestamp: bool = False,
        ) -> tuple[ProjectJob, ProjectJob] | None:
            reference_job_attribute = get_job_property(reference_job, property_name)
            refreshed_job_attribute = get_job_property(refreshed_job, property_name)
            if is_timestamp:
                reference_job_attribute = as_datetime(
                    reference_job_attribute
                ).timestamp()
                refreshed_job_attribute = as_datetime(
                    refreshed_job_attribute
                ).timestamp()
            if all(
                [
                    attr is None
                    for attr in [reference_job_attribute, refreshed_job_attribute]
                ]
            ):
                return None
            if reference_job_attribute is None:
                return refreshed_job, reference_job
            elif refreshed_job_attribute is None:
                return reference_job, refreshed_job
            if _reminder := numeric_sort(
                reference_job_attribute, refreshed_job_attribute
            ):
                if _reminder < 0:
                    newer, older = reference_job, refreshed_job
                else:
                    newer, older = refreshed_job, reference_job
                logger.debug(
                    "Refresh on %s the status of %d: %s from %s to %s",
                    self,
                    job_id,
                    property_name,
                    get_job_property(older, property_name),
                    get_job_property(newer, property_name),
                )
                return newer, older
            else:
                # they are the same, return first the reference_job to skip update
                return reference_job, refreshed_job

        def timestamp_fields_sort() -> tuple[ProjectJob, ProjectJob]:
            for property_name in ["finished_at", "started_at", "created_at"]:
                if pair := numeric_property_sort(property_name, is_timestamp=True):
                    newer, older = pair
                    return newer, older
            # at this level of coincidence we can consider them the same
            # then return the reference_job first to skip update
            return reference_job, refreshed_job

        job_id = reference_job.id

        reference_job_status = get_job_status(reference_job)
        refreshed_job_status = get_job_status(refreshed_job)

        if any(
            [
                reference_job_status == JobStatus.unknown,
                refreshed_job_status == JobStatus.unknown,
            ]
        ):
            # if any is "unknown" further check is required
            logger.warning(
                "Unknown job status. When refreshing %s, job %d found with a transition from %s to %s",
                self,
                job_id,
                reference_job.status,
                refreshed_job.status,
            )
            return timestamp_fields_sort()
        if reminder := numeric_sort(
            reference_job_status, refreshed_job_status
        ):  # they have different status
            if reminder < 0:
                newer_job, older_job = reference_job, refreshed_job
            else:
                newer_job, older_job = refreshed_job, reference_job
            logger.info(
                "Refresh on %s the status of %d: from %s to %s",
                self,
                job_id,
                older_job.status,
                newer_job.status,
            )
            return newer_job, older_job
        if reference_job_status == JobStatus.pending:
            if pair := numeric_property_sort("queue_duration"):
                return pair
        if reference_job_status == JobStatus.running:
            if pair := numeric_property_sort("duration"):
                return pair
        # at this point, they have the same status, and it's not unknown, pending, neither running
        # we can consider them the same, so return first the reference_job to skip update
        return reference_job, refreshed_job

    def __insert_job(self, job: ProjectJob) -> None:
        """
        Add a new sample of ProjectJob to the references in the object, maintaining retry storage consistency.
        When there is no retries to be stored the expectations and artifacts information is reset.
        :param job:
        :return:
        """
        job_id: int = job.id
        if self.__exclude_retried:
            previous_job = self.__gl_job[self.__latest_try]
            previous_job_id = previous_job.id
            self.__gl_job = {job_id: job}
            self.__latest_try = job_id
            self.__new_latest_try = True if previous_job_id != job_id else False
            self.__latest_try_previous_status = (
                previous_job.status
                if previous_job_id == job_id and previous_job.status != job.status
                else None
            )
            self.__expectation_changes = defaultdict(set)
            self.__duplicated_results = defaultdict(set)
            self.__artifacts_zip = {}
        else:
            previous_latest_try_id = self.__latest_try
            previous_latest_try_status = self.__gl_job[previous_latest_try_id].status
            self.__gl_job[job_id] = job
            self.__latest_try = max(self.__gl_job.keys())
            self.__new_latest_try = (
                True if previous_latest_try_id != self.__latest_try else False
            )
            self.__latest_try_previous_status = (
                previous_latest_try_status
                if previous_latest_try_id == self.__latest_try
                else None
            )

    def __getitem__(self, job_id: int) -> ProjectJob:
        if job_id not in self.__gl_job.keys():
            raise KeyError(f"Job id {job_id} not present.")
        return self.__gl_job[job_id]

    @property
    def name(self) -> str:
        name = self.__gl_job[self.__latest_try].name

        if isinstance(name, str):
            return name
        else:
            raise TypeError(f"Expected str, got {name!r}")

    @property
    def root_name(self) -> str:
        return self.__root_job_name()

    @property
    def id(self) -> int:
        job_id = self.__gl_job[self.__latest_try].id

        if isinstance(job_id, int):
            return job_id
        else:
            raise TypeError(f"Expected int, got {job_id!r}")

    @property
    def web_url(self) -> str:
        return self.__web_url_by_id(self.__latest_try)

    @property
    def stage(self) -> str:
        stage = self.__gl_job[self.__latest_try].stage

        if isinstance(stage, str):
            return stage
        else:
            raise TypeError(f"Expected str, got {stage!r}")

    @property
    def allow_failure(self) -> bool:
        return self.__allow_failure_by_id(self.__latest_try)

    @property
    def is_from_build_stage(self) -> bool:
        return is_build_stage(
            self.__project_name, self.__gl_job[self.__latest_try].stage
        )

    @property
    def created_at(self) -> datetime | None:
        return self.__created_at_by_id(self.__latest_try)

    @property
    def started_at(self) -> datetime | None:
        return self.__started_at_by_id(self.__latest_try)

    @property
    def finished_at(self) -> datetime | None:
        return self.__finished_at_by_id(self.__latest_try)

    @property
    def duration(self) -> timedelta:
        return self.__duration_by_id(self.__latest_try)

    @property
    def queue_duration(self) -> timedelta:
        return self.__queue_duration_by_id(self.__latest_try)

    @property
    def status(self) -> str:
        return self.__status_by_id(self.__latest_try)

    @property
    def previous_status(self) -> str | None:
        return self.__latest_try_previous_status

    @property
    def status_changed(self) -> bool:
        if self.__latest_try_previous_status is None:
            return False
        return self.__latest_try_previous_status != self.status

    @property
    def latest_try_changed(self) -> bool:
        return self.__new_latest_try

    @property
    def attributes(self) -> dict[str, Any]:
        return self.__gl_job[self.__latest_try].attributes

    @property
    def ids(self) -> list[int]:
        ids = list(self.__gl_job.keys())
        ids.sort()
        return ids

    @property
    def has_retries(self) -> bool:
        return len(self.__gl_job) > 1

    @property
    def has_expectation_changes(self) -> bool:
        """
        Boolean to know if there are expectations to change in the job.
        :return: True when there are expectations to update
        """
        return len(self.__expectation_changes) > 0

    @property
    def is_known_where_to_change_expectations(self) -> bool:
        return (
            self.__expectation_files_path is not None
            and self.__expectation_files_prefix is not None
        )

    @property
    def expectation_changes(self) -> dict[ExpectationCategories, set[str]]:
        """
        Dictionary with the expectations that are recommended to change in the job tests.
        :return: Dictionary with the test result and the tests that finished with this result.
        """
        return dict(self.__expectation_changes)

    @property
    def expectations_path(self) -> str | None:
        return self.__expectation_files_path

    @property
    def expectations_prefix(self) -> str | None:
        return self.__expectation_files_prefix

    def web_url_by_id(self, job_id: int) -> str:
        if job_id not in self.ids:
            raise KeyError(f"{job_id} not in {self.ids}")
        return self.__web_url_by_id(job_id)

    def __web_url_by_id(self, job_id: int) -> str:
        web_url = self.__gl_job[job_id].web_url
        if isinstance(web_url, str):
            return web_url
        else:
            raise TypeError(f"Expected str, got {web_url!r}")

    def allow_failure_by_id(self, job_id: int) -> bool:
        if job_id not in self.ids:
            raise KeyError(f"{job_id} not in {self.ids}")
        return self.__allow_failure_by_id(job_id)

    def __allow_failure_by_id(self, job_id: int) -> bool:
        allow_failure = self.__gl_job[job_id].allow_failure
        if isinstance(allow_failure, bool):
            return allow_failure
        else:
            raise TypeError(f"Expected bool, got {allow_failure!r}")

    def created_at_by_id(self, job_id: int) -> datetime | None:
        if job_id not in self.ids:
            raise KeyError(f"{job_id} not in {self.ids}")
        return self.__created_at_by_id(job_id)

    def __created_at_by_id(self, job_id: int) -> datetime | None:
        created_at = self.__gl_job[job_id].created_at
        if isinstance(created_at, str):
            return as_datetime(created_at)
        else:
            raise TypeError(f"Expected str, got {created_at!r}")

    def started_at_by_id(self, job_id: int) -> datetime | None:
        if job_id not in self.ids:
            raise KeyError(f"{job_id} not in {self.ids}")
        return self.__started_at_by_id(job_id)

    def __started_at_by_id(self, job_id: int) -> datetime | None:
        started_at = self.__gl_job[job_id].started_at
        if isinstance(started_at, str):
            return as_datetime(started_at)
        else:
            raise TypeError(f"Expected str, got {started_at!r}")

    def finished_at_by_id(self, job_id: int) -> datetime | None:
        if job_id not in self.ids:
            raise KeyError(f"{job_id} not in {self.ids}")
        return self.__finished_at_by_id(job_id)

    def __finished_at_by_id(self, job_id: int) -> datetime | None:
        finished_at = self.__gl_job[job_id].finished_at
        if isinstance(finished_at, str):
            return as_datetime(finished_at)
        else:
            raise TypeError(f"Expected str, got {finished_at!r}")

    def duration_by_id(self, job_id: int) -> timedelta:
        if job_id not in self.ids:
            raise KeyError(f"{job_id} not in {self.ids}")
        return self.__duration_by_id(job_id)

    def __duration_by_id(self, job_id: int) -> timedelta:
        duration = timedelta(seconds=self.__gl_job[job_id].duration)
        if isinstance(duration, timedelta):
            return duration
        else:
            raise TypeError(f"Expected float, got {duration!r}")

    def queue_duration_by_id(self, job_id: int) -> timedelta:
        if job_id not in self.ids:
            raise KeyError(f"{job_id} not in {self.ids}")
        return self.__queue_duration_by_id(job_id)

    def __queue_duration_by_id(self, job_id: int) -> timedelta:
        queued_duration = timedelta(seconds=self.__gl_job[job_id].queued_duration)
        if isinstance(queued_duration, timedelta):
            return queued_duration
        else:
            raise TypeError(f"Expected float, got {queued_duration!r}")

    def status_by_id(self, job_id: int) -> str:
        if job_id not in self.ids:
            raise KeyError(f"{job_id} not in {self.ids}")
        return self.__status_by_id(job_id)

    def __status_by_id(self, job_id: int) -> str:
        status = self.__gl_job[job_id].status
        if isinstance(status, str):
            return status
        else:
            raise TypeError(f"Expected str, got {status!r}")

    def trace(self, job_id: int | None = None) -> str:
        """
        Query the traces log of the job.
        :param job_id:
        :return: string with the job trace log
        """
        if job_id is not None and job_id not in self.ids:
            raise KeyError
        if trace := self.__get_trace(job_id):
            return trace
        raise FileNotFoundError("Trace not found")

    # TODO: list the available artifacts

    def get_artifact(self, artifact_name: str, job_id: int | None = None) -> Artifact:
        """
        Query the artifact file of the job.
        :param artifact_name:
        :param job_id:
        :return: string with the content of the file interpreted as string
        """
        header_msg = f"Get artifact {artifact_name} for {self.name} ({job_id=})"
        if job_id is not None and job_id not in self.ids:
            exception_msg = f"Job {job_id} is not in {self.ids}"
            logger.debug(
                "%s: %s",
                header_msg,
                exception_msg,
            )
            raise KeyError(exception_msg)
        job_id = self.__latest_try if job_id is None else job_id
        answer = self.__get_artifact(artifact_name, job_id)
        if isinstance(answer, Artifact):
            return answer
        elif isinstance(answer, Exception):
            if isinstance(answer, GitlabGetError) and answer.response_code == 404:
                return Artifact(
                    artifact_name,
                    exception=FileNotFoundError(f"Artifact {artifact_name} not found"),
                )
            return Artifact(artifact_name, exception=answer)

    def expectations_update(
        self,
        repo: Repository,
        path: str,
        file_prefix: str,
        use_results_pass: bool = False,
        artifacts: list[str] | None = None,
        xfiles_block_header: list[str] | None = None,
    ) -> list[set[ExpectationCategories]] | None:
        """
        Process the results (results.csv and failures.csv) from all the executions
        of the job, to update the expectation files associated with the job.
        :param repo: object managing where the expectations are stored
        :param path: path, in the repository to where the expectations are
        :param file_prefix: prefix on the file before they end with {fails,flakes,skips}.txt
        :param use_results_pass: flag to advice the ExpectationsProcessor to interpret Pass result
        :param artifacts: list of files used as source of expectation changes.
        :param xfiles_block_header: list if strings used to identify expectations block (or create it)
        :return: If there are multiple runs of the job, and some test had some inconsistent
                 results that this tool didn't handle, return a list to summarize them at the end.
        """
        # TODO: when the jobs support to report in the artifacts where their expectations are, this transfer of
        #  information will no longer be necessary.
        self.__expectation_files_path = path
        self.__expectation_files_prefix = file_prefix
        artifacts = ["*/results.csv.zst"] if artifacts is None else artifacts
        job_retries = len(self.ids)
        retries_str = f"{job_retries} runs" if job_retries > 1 else "1 run"
        logger.debug(
            "Job %s has %s",
            self.name,
            retries_str,
        )
        for job_execution_id in self.ids:
            logger.debug(
                "Get %s artifacts from job %s",
                self.name,
                job_execution_id,
            )
            for artifact_name in artifacts:
                artifact = self.get_artifact(artifact_name, job_execution_id)
                if not artifact.has_content:
                    logger.warning(
                        "Ignore job %s from %s as it didn't " "generate artifact %s.",
                        job_execution_id,
                        self.name,
                        artifact_name,
                    )
                else:
                    content = artifact.content_as_str
                    if len(content) > 100:
                        content = f"{content[:100]}..."
                    logger.info(
                        "Append to %s from execution %s " "%s: %r",
                        self.name,
                        job_execution_id,
                        artifact.file_name,
                        content,
                    )
                    self.__append_expectation_changes(
                        artifact.content_as_str.splitlines()
                    )
                del artifact
        summary = []
        for test_result, test_list in self.__expectation_changes.items():
            summary.append(f"{test_result}: {len(test_list)}")
        logger.debug(
            "Job %s results %s",
            self.name,
            " ,".join(summary),
        )
        if self.__duplicated_results:
            logger.warning(
                "Job %s have different results (%s runs) on %s tests",
                self.name,
                job_retries,
                len(self.__duplicated_results.keys()),
            )
        expectations: ExpectationsFiles = repo.get_expectations_contents(
            self.__expectation_files_path, self.__expectation_files_prefix
        )
        if xfiles_block_header is not None:
            expectations.add_block_identifier(xfiles_block_header)
        unhandled_result_names = ExpectationsProcessor(
            self.name, expectations, self.__expectation_changes
        ).process_expectation_changes(use_results_pass)
        repo.apply_expectation_changes(expectations)
        return unhandled_result_names

    @staticmethod
    def __fail_tests_and_their_causes(
        fails_contents: list[str],
    ) -> dict[str, TestResult]:
        """
        From the list of lines in the fails file, convert it to a dictionary where
        the key is the test name, and the value is the test results and the line.
        We can later check if the test has failed for the same reason or another.
        :param fails_contents:
        :return:
        """
        dct: dict[str, TestResult] = {}
        for i, line in enumerate(fails_contents, start=1):
            if line.startswith("#") or line.count(",") == 0:
                continue  # ignore these lines
            test_name, test_result = line.strip().rsplit(",", 1)
            dct[test_name] = {"result": test_result, "line": i}
        return dct

    @cache
    def __get_trace(self, job_id: int | None = None) -> str | None:
        try:
            job_id = self.__latest_try if job_id is None else job_id
            trace = self.__gl_job[job_id].trace()
            if isinstance(trace, bytes):
                return trace.decode("UTF-8")
            else:
                raise TypeError(f"Expected bytes, got {trace!r}")
        except GitlabGetError as exception:
            if exception.response_code == 404:
                return None
            raise exception

    @cache
    def __get_artifact(
        self, artifact_pattern: str, job_id: int
    ) -> Artifact | Exception:
        if artifact_pattern.startswith("*/"):
            try:
                return self.__walk_artifacts(artifact_pattern[2:], job_id)
            except FileNotFoundError as exception:
                return exception
        else:
            try:
                artifact_content = self.__gl_job[job_id].artifact(artifact_pattern)
            except GitlabGetError as exception:
                code = exception.response_code
                if code == 404:
                    # It means the artifact doesn´t exist.
                    # go to check the next path with the generator
                    return exception
                raise exception
            except requests.exceptions.ChunkedEncodingError:
                # When the artifact doesn't exist, it was returning a
                # GitlabGetError with a 404 (Page Not Found). But it seems
                # newer release of gitlab raises a different exception.
                return GitlabGetError(response_code=404)

            if artifact_content is None or isinstance(artifact_content, bytes):
                return Artifact(artifact_pattern, content=artifact_content)
            else:
                raise TypeError(f"Expected None or bytes, got {artifact_content!r}")

    def __root_job_name(self) -> str:
        """
        Remove the suffix on the sharded jobs to have a single name for all of them.
        :return: original name before sharding
        """
        name = self.name
        # if job is sharded, it has a " n/m" at the end, remove it.
        if search := re_match(r"(.*) [0-9]+/[0-9]+$", name):
            name = search.group(1)
        # remove the '-full' suffix
        if search := re_match(r"(.*)[-_]full$", name):
            name = search.group(1)
        # when after "-full", we have the arch tag
        elif search := re_match(r"(.*)[-_]full(:.*)$", name):
            name = f"{search.group(1)}{search.group(2)}"
        return name

    def __walk_artifacts(self, artifact_name: str, job_id: int) -> Artifact:
        """
        Used to aggregate the content of the files in the artifacts of a job. First
        downloading all the artifacts, and then iterate the files in the packet to
        get the content.
        :param artifact_name:
        :param job_id:
        :return:
        """
        # TODO: evaluate if Artifact object could manage to store multiple files
        #  allow to work with them as if they are a single file, but also to
        #  distinguish what comes from each source.
        job = self.__gl_job[job_id]
        artifacts_zip = self.__get_artifacts_zip(job)
        artifact_content = b""
        artifact_found = False
        for element in artifacts_zip.filelist:
            if element.filename.count(artifact_name):
                logger.info(
                    "Artifact found: %s",
                    element.filename,
                )
                # Concatenate the content for all the artifact_name files
                artifact_content += artifacts_zip.read(element.filename)
                artifact_found = True
        if not artifact_found:
            raise FileNotFoundError(f"Artifact {artifact_name} not found")
        return Artifact(artifact_name, content=artifact_content)

    def __get_artifacts_zip(self, job: ProjectJob) -> ArtifactsZip:
        if job.id not in self.__artifacts_zip.keys():
            self.__artifacts_zip[job.id] = ArtifactsZip(job)
        else:
            logger.debug(
                "Reusing downloaded %s:%s artifacts to %s",
                job.name,
                job.id,
                self.__artifacts_zip[job.id].artifacts_zip_name,
            )
        return self.__artifacts_zip[job.id]

    def __append_expectation_changes(self, tests: list[str]) -> None:
        """
        From a list of test results record, store them in a dictionary where the key is the test result and the items
        are the tests under that result.
        :param tests: list of test records
        :return:
        """
        for test_record in tests:
            if test_record.startswith("#") or len(test_record) == 0:
                continue  # ignore comment and empty lines
            if test_record.count(",") == 1:
                name, status = test_record.split(",")
            elif test_record.count(",") == 2:
                name, status, _ = test_record.split(",")
            else:
                raise AssertionError(f"Cannot interpret the record {test_record!r}")
            self.__expectation_changes[ExpectationCategories(status)].add(name)
