#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from logging import getLogger
from tempfile import TemporaryDirectory
from typing import TYPE_CHECKING
from zipfile import ZipFile

from gitlab.v4.objects import ProjectJob
from pyzstd import decompress

if TYPE_CHECKING:
    from typing import Any
    from zipfile import ZipInfo

logger = getLogger(__name__)


class Artifact:
    def __init__(
        self,
        file_name: str,
        content: bytes | None = None,
        exception: Exception | None = None,
    ):
        self.__file_name = file_name
        self.__zst = False
        if self.__file_name.endswith(".zst"):
            self.__zst = True
        self.__content = content
        self.__exception = exception

    def __repr__(self) -> str:
        return self.content_as_str

    @property
    def file_name(self) -> str:
        return self.__file_name

    @property
    def has_content(self) -> bool:
        return self.__exception is None

    @property
    def exception_as_str(self) -> str:
        if self.__exception is not None:
            return str(self.__exception.args[0])
        else:
            return ""

    @property
    def content_as_bytes(self) -> bytes:
        if self.__exception is not None:
            return str(self.__exception.args[0]).encode()
        if self.__zst and self.__content is not None:
            return decompress(self.__content)
        if self.__content is not None:
            return self.__content
        return b""

    @property
    def content_as_str(self) -> str:
        if self.__exception is not None:
            return str(self.__exception.args[0])
        elif self.__content is not None:
            return self.content_as_bytes.decode("UTF-8")
        else:
            return ""


class ArtifactsZip:
    __job: ProjectJob | None = None
    __tmp: TemporaryDirectory[str] | None = None
    __filelist: list[ZipInfo] | None = None

    def __init__(self, job: ProjectJob):
        self.__job = job
        self.__download_zip_file()

    def __download_zip_file(self) -> None:
        self.__tmp = TemporaryDirectory()
        job_id = self.__job.id if self.__job is not None else -1
        job_name = self.__job.name if self.__job is not None else ""
        try:
            if self.__job is not None:
                with open(self.artifacts_zip_name, "wb") as f:
                    self.__job.artifacts(streamed=True, action=f.write)
        except Exception:
            raise FileNotFoundError(f"'artifacts.zip' not found for job {job_id}")
        logger.info(
            "Downloaded %s:%s artifacts to %s",
            job_name,
            job_id,
            self.artifacts_zip_name,
        )
        with ZipFile(self.artifacts_zip_name) as zip_file:
            zip_file = ZipFile(self.artifacts_zip_name)
            self.__filelist = zip_file.filelist

    @property
    def artifacts_zip_name(self) -> str:
        if self.__tmp is not None:
            return f"{self.__tmp.name}/artifacts.zip"
        else:
            raise ValueError("Temporary directory not initialized")

    @property
    def filelist(self) -> list[ZipInfo]:
        if self.__filelist is not None:
            return self.__filelist
        else:
            raise ValueError("ZipFile directory not initialized")

    def read(self, *args: Any, **kwargs: Any) -> bytes:
        with ZipFile(self.artifacts_zip_name) as zip_file:
            file_content = zip_file.read(*args, **kwargs)
        return file_content
