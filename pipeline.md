# ci-collate pipeline subcommand

This subcommand is meant to collect information from all the jobs of a given 
pipeline. Detailed information about the subcommand with 
`ci-collate pipeline --help`.

[[_TOC_]]

## collate artifacts

Using the token stored in the environment variable `GITLAB_TOKEN`, one can 
request the content on the artifacts of the file `results/junit.xml` for all 
the jobs in a pipeline. For example, the request of this file of all the jobs 
in the pipeline `https://gitlab.freedesktop.org/mesa/mesa/-/pipelines/989977` 
can be requested by:

```commandline
ci-collate \
    --namespace mesa \
    pipeline \
        --artifact results/junit.xml \
        989977
```

And from python:

```python
from pprint import pprint
from glcollate import Collate
pipeline = Collate(namespace='mesa').from_pipeline(989977)
artifacts = pipeline.get_artifact(artifact_name="results/junit.xml")
pprint(artifacts)
```

Where this `artifacts` object is a nested dictionary where the first level 
keys are the job names and in the second level we have jobs ids (to collect 
together the information when there are retries of the same job) and the leaf 
of the nested dictionary is a string with the content of the artifact (or a 
message that says it wasn't found).

Let's see what was for one of the jobs used in the example for the 
[job](./job.md) subcommand:

```python
pprint(artifacts['gc2000_gles2'])
{49215221: '<?xml version="1.0" encoding="utf-8"?>\n'
           '<testsuites>\n'
           '  <testsuite id="0" name="dEQP" package="testsuite/dEQP" tests="0" '
           'errors="0" failures="0" hostname="localhost" '
           'timestamp="2023-09-20T04:14:46.034475195+00:00" time="0" />\n'
           '</testsuites>'}
```

Or what one got when asking this to a build job:

```python
pprint(artifacts['debian/x86_64_build'])
{49215127: 'Artifact results/junit.xml not found'}
```

### Subsetting jobs: filters

When one is trying to collate information from a pipeline, sometimes the 
interest is not on all the jobs but a subset based on the state or the stage of 
the jobs, or a pattern (regex) in the names.

#### Filter by job status or stage

If one likes to collect all the `results/failures.csv` from failed jobs:

```commandline
ci-collate \
    --namespace mesa \
    pipeline \
        --artifact results/failures.csv \
        --status failed \
        989977
```

And from python:

```python
from glcollate import Collate
pipeline = Collate(namespace='mesa').from_pipeline(989977)
artifacts = pipeline.get_artifact("results/failures.csv", status="failed")
```

This filter can be used in conjunction (or not) of the stage filter, to get the
artifact content for the failed jobs in some particular stages:

```commandline
ci-collate \
    --namespace mesa \
    pipeline \
        --artifact results/failures.csv \
        --status failed \
        --stage etnaviv --stage freedreno \
        989977
```

```python
artifacts = pipeline.get_artifact("results/failures.csv", status="failed", stage=["etnaviv", "freedreno"])
```

Remark that in command line, to filter by many stages (or status) the option is
repeated as many times as wanted, when in the Python the argument is a list of 
strings.

#### Filter by job name (regex)

One maybe interested in a set of jobs that a regular expression could help to 
filter. In combination or not with the previous filters, one can specify a 
regular expression to subset the jobs by name.

As an example, to get the `results/junit.xml` file from all the jobs with a 
suffix `full` (note: this example comes from a __nightly run__ pipeline; 
another note: for the regular expression is used the __unsharded name__):

```commandline
ci-collate \
    --namespace mesa \
    pipeline \
        --artifact results/junit.xml \
        --job-filter .*full \
        989977
```

And from python:

```python
from glcollate import Collate
pipeline = Collate(namespace='mesa').from_pipeline(989977)
artifacts = pipeline.get_artifact("results/junit.xml", job_regex=".*full")
```

#### Information about job retries in pipelines

When one work with pipelines, there are jobs that could have been retried and 
the information from them could be the most interesting thing or extra info 
that confuses. So here has been established a way to differentiate if the user 
likes to work with or without this information.

When getting the `CollatePipeline` object one doesn't like to even collect this
information so don't gather this information from the jobs pipeline, they can 
call:

```python
from glcollate import Collate
Collate(namespace='gfx-ci-bot', project='virglrenderer').from_pipeline(833783, exclude_retried=True)
```

The default case, when this argument is not explicitly set to true, the 
`CollateJob` object in the `CollatePipeline` will store this information. If 
during the activity with the `CollatePipeline` object one doesn't like to 
interact with the retries, and only work with the last execution of the jobs, 
they can change the property `exclude_retried`.

To know if there are retried jobs in the pipeline, there is also a property 
listing them called `jobs_with_retries`.

So, for example, on this previous pipeline (building it with the default 
`exclude_retries` to `False`) we can access the set of jobs and there is one 
per job name but one can use them to see the retried instances:

```python
>>> pipeline.jobs(r"^piglit.*host")
 {CollateJob(piglit-gl-host), CollateJob(piglit-gles-host)}
>>> pipeline.jobs_with_retries
 {CollateJob(piglit-gles-host)}
>>> job = pipeline.jobs_with_retries.pop()
>>> job.ids
 [37399188, 37400671]
```

To get the trace from the retried job, one can specify it as an argument:
```python
print(job.trace(job_id=job.ids[0]))
```
When nothing is specified, it points to the last of the jobs.
