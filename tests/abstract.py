#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"


from operator import setitem
from os import environ as os_environ
from unittest import TestCase
from unittest.mock import patch

from mockgitlab import Gitlab, create_random_fake_token

from glcollate import Collate
from glcollate.glproxy import GitlabProxy


def _gitlab_builder(url, private_token=None):
    return Gitlab(url, private_token=private_token)


class CollateTestCase(TestCase):
    def _build_collate_object(self) -> Collate:
        fake_token = create_random_fake_token()
        self._prepare_environment({"GITLAB_TOKEN": fake_token})
        # force to create the build of the fake
        patcher = patch.object(GitlabProxy, "_gitlab_builder", _gitlab_builder)
        patcher.start()
        self.addCleanup(patcher.stop)
        return Collate()

    def _prepare_environment(self, environment: dict):
        for variable, value in environment.items():
            old_value = os_environ.get(variable)
            os_environ[variable] = value

            # Restore environ after test completes
            if old_value is not None:
                self.addCleanup(setitem, os_environ, variable, old_value)
            else:
                self.addCleanup(os_environ.pop, variable)
