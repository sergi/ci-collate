## Recording and replaying responses

The `tools/record.py` is a wrapper script around the glcollate CLI that will
record the HTTP requests and responses that the glcollate does. The recorded
responses can later be used in unit tests to have a reproducible offline environment.

It uses the **beta** feature from the `responses` module to provide a [record of 
the HTTP requests](https://pypi.org/project/responses/#record-responses-to-files)
that `python-gitlab` does, to a YAML file.

The script is completely independent, so it is not modifying the command line, 
but instead replaces the CLI call, to then produce as outcome a `out.yaml` file
in the current directory with the recorded HTTP calls.

Example use called from the project root folder:

```bash
PYTHONPATH=. python -B tools/record.py --namespace mesa pipeline --artifact results/failures.csv --status failed 1310796
```

The `PYTHONPATH` environment variable is required because the script
is located in the `tools/` folder therefore the root folder where
`glcollate` is located would not be added to package search path and
might raise ImportError or use system installed `glcollate`.
(the wrapper can work with system installed `glcollate` as well)

The output file, can be copied to the `tests/` directory with a meaning full 
name, and then write a test to use it to mock `python-gitlab` requests.

The example command from before is being replayed in this unit test:

```python
from __future__ import annotations

from unittest import TestCase, main

import responses

from glcollate import Collate


class TestPipeline(TestCase):
    def test_pipeline_artifacts(self) -> None:
        with responses.RequestsMock() as rsps:
            rsps._add_from_file("out.yaml")

            collate = Collate(
                gitlab_url="https://gitlab.freedesktop.org",
                gitlab_token_file_name="/dev/null",
                namespace="mesa",
                project="mesa",
            )
            pipeline = collate.from_pipeline(1310796)
            artifacts_dict = pipeline.get_artifact(
                "results/failures.csv",
                job_regex=None,
                status="failed",
                stage=None,
            )
            self.assertIsNotNone(artifacts_dict)


if __name__ == "__main__":
    main()

```
