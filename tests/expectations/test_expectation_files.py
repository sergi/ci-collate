#!/usr/bin/env python3.11

# Copyright (C) 2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2024 Collabora Ltd"


from unittest import TestCase

from test_expectations_processor import (
    FAILS_FILE_PATH,
    FLAKES_FILE_PATH,
    SKIPS_FILE_PATH,
)

from glcollate.expectations.files import ExpectationBlock, ExpectationFile


class ExpectationFileTestCase(TestCase):
    def test_empty_file(self) -> None:
        xfile = ExpectationFile("bar/foo.txt", [])
        assert xfile.is_empty == True

    def test_expectationfile_object(self) -> None:
        xfile = ExpectationFile(
            SKIPS_FILE_PATH.name, SKIPS_FILE_PATH.read_text().splitlines(keepends=True)
        )
        assert xfile.file_name == SKIPS_FILE_PATH.name
        assert xfile.is_empty == False
        assert xfile.modified == False
        assert xfile.n_blocks == 6
        assert xfile.n_records == 19
        assert len(xfile) == 45
        # TODO: check the accumulated number of lines in blocks meet the number of lines in the file
        # TODO: Test reconstruct the file using xfiles.as_lines

    def test_add_tests_to_expectations(self):
        xfile = ExpectationFile(
            FAILS_FILE_PATH.name, FAILS_FILE_PATH.read_text().splitlines(keepends=True)
        )
        first_crashing_tests = {
            "KHR-GLES2.core.internalformat.texture2d.depth_component_unsigned_int_depth_component24",
            "x11-dEQP-EGL.functional.create_context.no_config",
            "wayland-dEQP-EGL.functional.create_context.no_config",
        }
        second_crashing_tests = {
            "dEQP-GLES2.functional.clipping.line.wide_line_clip_viewport_center"
        }
        block_0 = xfile.blocks[0].content_as_lines
        block_1 = xfile.blocks[1].content_as_lines
        block_2 = xfile.blocks[2].content_as_lines
        last_block = xfile.blocks[-1].content_as_lines
        xfile.add_record(first_crashing_tests, "Crash")
        assert xfile.modified == True

        # block_0 has one register less
        assert xfile.blocks[0].content_as_lines != block_0
        assert xfile.blocks[0].content_as_lines == [
            "# Test expects red instead of luminance, contra OES_depth_texture spec.\n",
            "# https://gitlab.khronos.org/Tracker/vk-gl-cts/-/issues/3815\n",
            "KHR-GLES2.core.internalformat.texture2d.depth_component_unsigned_int_depth_component16,Fail\n",
            "KHR-GLES2.core.internalformat.texture2d.depth_component_unsigned_short_depth_component16,Fail\n",
            "\n",
        ]
        # all tests in block_1 are removed, then the block completely removed (including comments and separator)
        assert xfile.blocks[1].content_as_lines != block_1
        assert xfile.blocks[1].content_as_lines == block_2
        # new block with tests added at the end
        assert xfile.blocks[-1].content_as_lines != last_block
        assert xfile.blocks[-1].content_as_lines == [
            "KHR-GLES2.core.internalformat.texture2d.depth_component_unsigned_int_depth_component24,Crash\n",
            "wayland-dEQP-EGL.functional.create_context.no_config,Crash\n",
            "x11-dEQP-EGL.functional.create_context.no_config,Crash\n",
            "\n",
        ]
        # the block is sorted, so this second set must be added sorted
        xfile.add_record(second_crashing_tests, "Crash")
        assert xfile.blocks[1].content_as_lines != block_2
        assert xfile.blocks[1].content_as_lines == [
            "# wide line outside the viewport incorrectly clipped out when ES wants it\n",
            "# rendered as a quad and clipped appropriately.  I think by expanding\n",
            "# CLIPPER_XY_SCALING to have a guard band we might get these to work.\n",
            "dEQP-GLES2.functional.clipping.line.wide_line_clip_viewport_corner,Fail\n",
            "\n",
        ]
        assert xfile.blocks[-1].content_as_lines == [
            "KHR-GLES2.core.internalformat.texture2d.depth_component_unsigned_int_depth_component24,Crash\n",
            "dEQP-GLES2.functional.clipping.line.wide_line_clip_viewport_center,Crash\n",
            "wayland-dEQP-EGL.functional.create_context.no_config,Crash\n",
            "x11-dEQP-EGL.functional.create_context.no_config,Crash\n",
            "\n",
        ]
        # TODO: reconstruct the file and compare with a diff

    def test_remove_tests_from_expectations(self):
        xfile = ExpectationFile(
            FLAKES_FILE_PATH.name,
            FLAKES_FILE_PATH.read_text().splitlines(keepends=True),
        )
        non_flaky_tests = {
            # one from the first block
            "dEQP-GLES2.functional.clipping.triangle_vertex.clip_three.clip_neg_x_and_pos_x_and_pos_y_pos_z",
            # two from the second block
            "arm32-dEQP-GLES2.functional.texture.mipmap.cube.generate.rgb565_nicest",
            "arm32-dEQP-GLES2.functional.texture.mipmap.cube.generate.a8_nicest",
            # three from the third block
            "spec@ext_framebuffer_blit@fbo-sys-blit",
            "spec@ext_framebuffer_object@fbo-flushing-2",
            "glx@glx-visuals-stencil",
            # four from the fourth block
            "spec@!opengl 1.1@depthstencil-default_fb-drawpixels-24_8 samples=2",
            "spec@!opengl 1.1@depthstencil-default_fb-drawpixels-24_8 samples=4",
            "spec@!opengl 1.1@depthstencil-default_fb-drawpixels-float-and-ushort samples=4",
            "spec@!opengl 1.1@depthstencil-default_fb-clear samples=2",
            # all from the last block, so remove the comment also
            "dEQP-EGL.functional.sharing.gles2.multithread.random.programs.link.19",
            "dEQP-EGL.functional.sharing.gles2.multithread.random_egl_sync.programs.link.19",
        }
        block_0 = xfile.blocks[0].content_as_lines
        block_1 = xfile.blocks[1].content_as_lines
        block_2 = xfile.blocks[2].content_as_lines
        block_3 = xfile.blocks[3].content_as_lines
        block_last_2 = xfile.blocks[-2].content_as_lines
        block_last_1 = xfile.blocks[-1].content_as_lines
        xfile.remove_records(non_flaky_tests)
        assert xfile.modified == True
        assert xfile.blocks[0].content_as_lines != block_0
        assert xfile.blocks[0].content_as_lines == [
            "dEQP-GLES2.functional.clipping.triangle_vertex.clip_three.clip_neg_x_and_pos_x_and_neg_x_neg_y_neg_z\n",
            "dEQP-GLES2.functional.clipping.triangle_vertex.clip_three.clip_neg_x_and_pos_x_neg_y_pos_z_and_neg_x_pos_y_neg_z\n",
            "dEQP-GLES2.functional.clipping.triangle_vertex.clip_three.clip_pos_x_and_neg_x_neg_y_pos_z_and_neg_x_pos_y_neg_z\n",
            "\n",
        ]
        assert xfile.blocks[1].content_as_lines != block_1
        assert xfile.blocks[1].content_as_lines == [
            "arm32-dEQP-GLES2.functional.shaders.random.texture.vertex.44\n",
            "arm32-dEQP-GLES2.functional.texture.mipmap.cube.generate.l8_nicest\n",
            "arm32-dEQP-GLES2.functional.texture.mipmap.cube.generate.rgb888_nicest\n",
            "arm32-dEQP-GLES2.functional.texture.mipmap.cube.generate.rgba4444_nicest\n",
            "arm32-dEQP-GLES2.functional.texture.mipmap.cube.generate.rgba5551_nicest\n",
            "\n",
        ]
        assert xfile.blocks[2].content_as_lines != block_2
        assert xfile.blocks[2].content_as_lines == [
            "glx@glx-multi-window-single-context\n",
            "shaders@glsl-vs-loop\n",
            "shaders@glsl-vs-loop-nested\n",
            "spec@ext_framebuffer_blit@fbo-sys-sub-blit\n",
            "spec@egl_chromium_sync_control@conformance\n",
            "\n",
        ]
        assert xfile.blocks[3].content_as_lines != block_3
        assert xfile.blocks[3].content_as_lines == [
            "# CMA allocations that may sometimes succeed\n",
            "spec@!opengl 1.1@depthstencil-default_fb-drawpixels-float-and-ushort samples=2\n",
            "\n",
        ]
        # last block had removed all the records, so remove the comments and separator.
        # then the last one becomes what was the one before it.
        assert xfile.blocks[-1].content_as_lines != block_last_1
        assert xfile.blocks[-1].content_as_lines == block_last_2

    def test_add_tests_to_a_new_block_with_header(self) -> None:
        xfile = ExpectationFile(
            SKIPS_FILE_PATH.name, SKIPS_FILE_PATH.read_text().splitlines(keepends=True)
        )
        xfile.new_records_block_identifier = ["# comment", "# second comment line"]
        xfile.add_record(
            {
                "spec@arb_draw_elements_base_vertex@arb_draw_elements_base_vertex-negative-index"
            }
        )
        assert xfile.blocks[-1].content_as_lines == [
            "# comment\n",
            "# second comment line\n",
            "spec@arb_draw_elements_base_vertex@arb_draw_elements_base_vertex-negative-index\n",
            "\n",
        ]

    def test_add_tests_to_an_existing_block_by_header(self) -> None:
        xfile = ExpectationFile(
            SKIPS_FILE_PATH.name, SKIPS_FILE_PATH.read_text().splitlines(keepends=True)
        )
        xfile.new_records_block_identifier = [
            "# This is causing a binning memory overflow problem"
        ]
        xfile.add_record(
            {
                "a@fake.before.dEQP",
                "spec@arb_draw_elements_base_vertex@arb_draw_elements_base_vertex-negative-index",
            }
        )
        assert xfile.blocks[1].content_as_lines == [
            "# This is causing a binning memory overflow problem\n",
            "a@fake.before.dEQP\n",
            "dEQP-GLES2.functional.fragment_ops.scissor.outside_render_line\n",
            "spec@arb_draw_elements_base_vertex@arb_draw_elements_base_vertex-negative-index\n",
            "\n",
        ]


sorted_raw_block = [
    "a\n",
    "e\n",
    "i\n",
    "o\n",
    "u\n",
]

unsorted_raw_block = [
    "q\n",
    "w\n",
    "e\n",
    "r\n",
    "t\n",
    "y\n",
]

another_unsorted_raw_block = [
    "a\n",
    "s\n",
    "e\n",
    "r\n",
    "t\n",
    "y\n",
]


def append_states_to_records(raw_block: list[str], state: str = "Fail") -> list[str]:
    new_raw_block: list[str] = []
    for record in raw_block:
        new_raw_block.append(f"{record.strip()},{state}\n")
    return new_raw_block


class ExpectationBlockTestCase(TestCase):
    def test_simple_creation_wo_state(self) -> None:
        # using unsorted raw block
        block_content = ["#comment\n"] + unsorted_raw_block + ["\n"]
        block = ExpectationBlock(block_content)
        assert block.content_as_lines == [
            "#comment\n",
            "q\n",
            "w\n",
            "e\n",
            "r\n",
            "t\n",
            "y\n",
            "\n",
        ]
        assert block.records == list("qwerty")
        assert block.n_records == len("qwerty")
        assert block.is_block_sorted() == False
        assert len(block) == len(block_content)

    def test_simple_creation_with_state(self) -> None:
        block_content = (
            ["#comment\n"] + append_states_to_records(unsorted_raw_block) + ["\n"]
        )
        block = ExpectationBlock(block_content)
        assert block.content_as_lines == [
            "#comment\n",
            "q,Fail\n",
            "w,Fail\n",
            "e,Fail\n",
            "r,Fail\n",
            "t,Fail\n",
            "y,Fail\n",
            "\n",
        ]
        assert block.records == list("qwerty")
        assert block.n_records == len("qwerty")
        assert block.is_block_sorted() == False
        assert len(block) == len(
            ["#comment\n"] + append_states_to_records(unsorted_raw_block) + ["\n"]
        )

    def test_multiple_header_lines_creation(self) -> None:
        # using sorted raw block
        block_content = (
            ["#comment\n", "# second comment line\n"] + sorted_raw_block + ["\n"]
        )
        block = ExpectationBlock(block_content)
        assert block.content_as_lines == [
            "#comment\n",
            "# second comment line\n",
            "a\n",
            "e\n",
            "i\n",
            "o\n",
            "u\n",
            "\n",
        ]
        assert block.records == list("aeiou")
        assert block.n_records == len("aeiou")
        assert block.is_block_sorted() == True
        assert len(block) == len(block_content)

    def test_comment_in_the_middle(self) -> None:
        block_content = unsorted_raw_block + ["#comment\n"] + sorted_raw_block + ["\n"]
        block = ExpectationBlock(block_content)
        # If the constructor input have duplicated, it is not sanitizing
        #  as this objects are agnostic on the input, and only do changes
        #  when they are requested explicitly
        assert block.content_as_lines == [
            "q\n",
            "w\n",
            "e\n",
            "r\n",
            "t\n",
            "y\n",
            "#comment\n",
            "a\n",
            "e\n",
            "i\n",
            "o\n",
            "u\n",
            "\n",
        ]
        assert block.records == list("qwerty") + list("aeiou")
        assert block.n_records == len("qwerty") + len("aeiou")
        assert block.is_block_sorted() == False
        assert len(block) == len(block_content)

    def test_without_empty_lines_at_the_end(self) -> None:
        block_content = ["#comment\n"] + unsorted_raw_block
        block = ExpectationBlock(block_content)
        assert block.content_as_lines == [
            "#comment\n",
            "q\n",
            "w\n",
            "e\n",
            "r\n",
            "t\n",
            "y\n",
        ]
        assert block.records == list("qwerty")
        assert block.n_records == len("qwerty")
        assert block.is_block_sorted() == False
        assert len(block) == len(block_content)

    def test_multiple_empty_lines_at_the_end(self) -> None:
        block_content = ["#comment\n"] + sorted_raw_block + ["\n"] * 3
        block = ExpectationBlock(block_content)
        assert block.content_as_lines == [
            "#comment\n",
            "a\n",
            "e\n",
            "i\n",
            "o\n",
            "u\n",
            "\n",
            "\n",
            "\n",
        ]
        assert block.records == list("aeiou")
        assert block.n_records == len("aeiou")
        assert block.is_block_sorted() == True
        assert len(block) == len(block_content)

    def test_search_records(self) -> None:
        block = ExpectationBlock(
            ["#comment\n", "# second comment line\n"]
            + sorted_raw_block
            + ["#middle comment"]
            + unsorted_raw_block
            + ["\n"] * 2
        )
        # set("aeiou") | set("qwerty")
        found, not_found = block.find_records(set(another_unsorted_raw_block))
        assert found == {"a", "e", "r", "t", "y"}
        assert not_found == {"s"}

    def test_append_records_unsorted(self) -> None:
        block = ExpectationBlock(["#comment\n"] + unsorted_raw_block + ["\n"] * 3)
        records_added = block.append_records(set(another_unsorted_raw_block))
        # set("qwerty") | set("aserty")
        assert records_added == set("as")
        assert block.content_as_lines == [
            "#comment\n",
            "q\n",
            "w\n",
            "e\n",
            "r\n",
            "t\n",
            "y\n",
            "a\n",
            "s\n",
            "\n",
            "\n",
            "\n",
        ]
        assert block.records == ["q", "w", "e", "r", "t", "y", "a", "s"]

    def test_append_records_sorted(self) -> None:
        block = ExpectationBlock(
            ["#comment\n", "# second comment line\n"] + sorted_raw_block + ["\n"] * 3
        )
        records_added = block.append_records(set(another_unsorted_raw_block))
        # set("aeiou") | set("aserty")
        assert records_added == set("srty")
        assert block.content_as_lines == [
            "#comment\n",
            "# second comment line\n",
            "a\n",
            "e\n",
            "i\n",
            "o\n",
            "r\n",
            "s\n",
            "t\n",
            "u\n",
            "y\n",
            "\n",
            "\n",
            "\n",
        ]
        assert block.records == ["a", "e", "i", "o", "r", "s", "t", "u", "y"]

    def test_append_records_with_state(self) -> None:
        block = ExpectationBlock([])
        records_added = block.append_records(set(unsorted_raw_block), "Fail")
        assert records_added == set("qwerty")

    def test_remove_records(self) -> None:
        block = ExpectationBlock(
            ["#comment\n", "# second comment line\n"]
            + sorted_raw_block
            + ["#middle comment\n"]
            + unsorted_raw_block
            + ["\n"] * 2
        )
        removed, not_found = block.remove_records(set(another_unsorted_raw_block))
        # (set("aeiou") | set("qwerty")).discard(set("aserty")
        assert removed == set("aerty")
        assert not_found == set("s")
        assert block.content_as_lines == [
            "#comment\n",
            "# second comment line\n",
            "i\n",
            "o\n",
            "u\n",
            "#middle comment\n",
            "q\n",
            "w\n",
            "\n",
            "\n",
        ]
        assert block.records == ["i", "o", "u", "q", "w"]
