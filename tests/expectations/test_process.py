from itertools import repeat
from unittest.mock import MagicMock

import pytest

from glcollate.expectations import ExpectationsProcessor

ALL_FILES = {"fails", "flakes", "skips"}


@pytest.fixture
def expectations_mock():
    return MagicMock()


EXPECTATIONS_SCENARIOS = {
    "test_fail_to_flake": {
        "test_category": "Flake",
        "expected_file_removals": ["fails"],
        "expected_file_additions": ["flakes"],
    },
    "test_fail_to_pass": {
        # fail -> pass
        # formerly known as "UnexpectedPass"
        "test_category": "UnexpectedImprovement(Pass)",
        "expected_file_removals": ["fails"],
        "expected_file_additions": [],
    },
    "test_timeout": {
        "test_category": "Timeout",
        "expected_file_removals": ["fails", "flakes"],
        "expected_file_additions": ["skips"],
    },
    "test_new_fail": {
        "test_category": "Fail",
        "expected_file_removals": [],
        "expected_file_additions": ["fails"],
        "new_status": ["Fail"],
    },
    "test_fail_to_skip": {
        # fail -> skip
        # eg.: correctly detect that a driver does not support a feature
        "test_category": "UnexpectedImprovement(Skip)",
        "expected_file_removals": ["fails"],
        "expected_file_additions": [],
    },
    "test_crash_to_fail": {
        # crash -> fail
        "test_category": "UnexpectedImprovement(Fail)",
        "expected_file_removals": ["fails"],
        "expected_file_additions": ["fails"],
        "new_status": ["Fail"],
    },
}


@pytest.mark.parametrize(
    "test_case", EXPECTATIONS_SCENARIOS.values(), ids=EXPECTATIONS_SCENARIOS.keys()
)
def test_expectation_file_updates(expectations_mock, test_case, request):
    """
    Test the ExpectationsProcessor's ability to update expectation files.

    This test function simulates the processing of expectation changes and verifies
    that the correct records are added and removed from the appropriate expectation files.

    Parameters:
    expectations_mock (Mock): A mock Expectations object that simulates the expectation files.
    test_case (dict): A dictionary containing the test case data. This includes the test category,
                      expected file additions and removals, and the new status for each test.
    request (FixtureRequest): A pytest fixture request object. Used to get the name of the current
                              test.
    """
    test_name = request.node.name
    processor = ExpectationsProcessor(
        "test_job",
        expectations_mock,
        {
            test_case["test_category"]: {test_name},
        },
    )

    processor.process_expectation_changes()

    expected_file_removals = test_case.get("expected_file_removals", [])
    expected_file_additions = test_case.get("expected_file_additions", [])
    intact_removal_files = ALL_FILES - set(expected_file_removals)
    intact_addition_files = ALL_FILES - set(expected_file_additions)

    # Asserts for removals
    for file_name in expected_file_removals:
        mock_attribute = getattr(expectations_mock, file_name)
        mock_attribute.remove_records.assert_called_with({test_name})

    # Assert that no other file was touched
    for file_name in intact_removal_files:
        mock_attribute = getattr(expectations_mock, file_name)
        mock_attribute.remove_records.assert_not_called()
    for file_name in intact_addition_files:
        mock_attribute = getattr(expectations_mock, file_name)
        mock_attribute.add_record.assert_not_called()

    # Asserts for additions
    new_stata = test_case.get("new_status", repeat(None))
    for file_name, new_status in zip(
        test_case.get("expected_file_additions", []), new_stata
    ):
        mock_attribute = getattr(expectations_mock, file_name)
        if new_status:
            mock_attribute.add_record.assert_called_once_with(
                {test_name}, test_result=new_status
            )
            continue
        mock_attribute.add_record.assert_called_once_with({test_name})


def test_unmanaged_categories(caplog, expectations_mock):
    processor = ExpectationsProcessor(
        "test_job", expectations_mock, {"UnknownCategory": {"test1"}}
    )

    processor.process_expectation_changes()

    assert "UnknownCategory" in caplog.text
