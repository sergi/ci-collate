# Copyright (C) 2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

from collections import defaultdict
from pathlib import Path
from unittest import TestCase

from glcollate.expectations import (
    ExpectationCategories,
    ExpectationFile,
    ExpectationsFiles,
    ExpectationsProcessor,
)
from glcollate.expectations.process import logger

EXPECTATIONS_FOLDER = Path(__file__).parent

FAILS_FILE_PATH = EXPECTATIONS_FOLDER / "broadcom-rpi3-fails.txt"
FLAKES_FILE_PATH = EXPECTATIONS_FOLDER / "broadcom-rpi3-flakes.txt"
SKIPS_FILE_PATH = EXPECTATIONS_FOLDER / "broadcom-rpi3-skips.txt"


class TestExpectationsProcessor(TestCase):
    def setUp(self) -> None:
        self.fails_expectations = ExpectationFile(
            FAILS_FILE_PATH.name,
            FAILS_FILE_PATH.read_text().splitlines(keepends=True),
        )
        self.flakes_expectations = ExpectationFile(
            FLAKES_FILE_PATH.name,
            FLAKES_FILE_PATH.read_text().splitlines(keepends=True),
        )
        self.skips_expectations = ExpectationFile(
            SKIPS_FILE_PATH.name,
            SKIPS_FILE_PATH.read_text().splitlines(keepends=True),
        )

        self.expectations_files = ExpectationsFiles()
        self.expectations_files.insert("fails", self.fails_expectations)
        self.expectations_files.insert("flakes", self.flakes_expectations)
        self.expectations_files.insert("skips", self.skips_expectations)

        self.expectation_changes: defaultdict[
            ExpectationCategories, set[str]
        ] = defaultdict(set)

    def assert_expectation_present(
        self,
        expectation_str: str,
        expectation_file: ExpectationFile,
    ) -> str:
        for line in expectation_file.as_lines:
            record: str = line.strip()
            if record.count(","):
                record, _ = record.rsplit(",", 1)
            if record == expectation_str:
                return line

        raise self.failureException(
            f"expectation {expectation_str!r} not found in {expectation_file!r}"
        )

    def assert_expectation_missing(
        self,
        expectation_str: str,
        expectation_file: ExpectationFile,
    ) -> None:
        for line in expectation_file.as_lines:
            record: str = line.strip()
            if record.count(","):
                record, _ = record.rsplit(",", 1)
            if record == expectation_str:
                raise self.failureException(
                    f"expectation {expectation_str!r} found in {expectation_file!r}"
                )

    def create_expectations_processor(self) -> ExpectationsProcessor:
        return ExpectationsProcessor(
            "vc4-rpi3-gl:arm32",
            self.expectations_files,
            self.expectation_changes,
        )

    def test_fail_to_flake(self) -> None:
        test_name_to_flake = (
            "dEQP-GLES2.functional.clipping.line.wide_line_clip_viewport_center"
        )

        self.expectation_changes[ExpectationCategories.FLAKE] = {test_name_to_flake}

        self.assert_expectation_present(test_name_to_flake, self.fails_expectations)

        processor = self.create_expectations_processor()
        processor.process_expectation_changes()

        self.assert_expectation_missing(test_name_to_flake, self.fails_expectations)
        self.assert_expectation_present(test_name_to_flake, self.flakes_expectations)

    def test_fail_to_pass(self) -> None:
        test_name_to_pass = (
            "spec@glsl-1.20@execution@tex-miplevel-selection gl2:texture() 3d"
        )

        self.expectation_changes[ExpectationCategories.UNEXPECTED_IMPROVEMENT_PASS] = {
            test_name_to_pass
        }

        self.assert_expectation_present(test_name_to_pass, self.fails_expectations)

        processor = self.create_expectations_processor()
        processor.process_expectation_changes()

        self.assert_expectation_missing(test_name_to_pass, self.fails_expectations)
        self.assert_expectation_missing(test_name_to_pass, self.flakes_expectations)
        self.assert_expectation_missing(test_name_to_pass, self.skips_expectations)

    def test_fail_to_pass_no_use_pass_results(self) -> None:
        test_name_to_pass = (
            "spec@ext_framebuffer_multisample@accuracy 2 depth_draw depthstencil"
        )

        self.expectation_changes[ExpectationCategories.PASS] = {test_name_to_pass}

        self.assert_expectation_present(test_name_to_pass, self.fails_expectations)

        processor = self.create_expectations_processor()
        processor.process_expectation_changes()

        self.assert_expectation_present(test_name_to_pass, self.fails_expectations)
        self.assert_expectation_missing(test_name_to_pass, self.flakes_expectations)
        self.assert_expectation_missing(test_name_to_pass, self.skips_expectations)

    def test_fail_to_pass_use_pass_results(self) -> None:
        test_name_to_pass = (
            "spec@ext_framebuffer_multisample@accuracy 2 depth_draw depthstencil"
        )

        self.expectation_changes[ExpectationCategories.PASS] = {test_name_to_pass}

        self.assert_expectation_present(test_name_to_pass, self.fails_expectations)

        processor = self.create_expectations_processor()
        processor.process_expectation_changes(True)

        self.assert_expectation_missing(test_name_to_pass, self.fails_expectations)
        self.assert_expectation_missing(test_name_to_pass, self.flakes_expectations)
        self.assert_expectation_missing(test_name_to_pass, self.skips_expectations)

    def test_to_timeout(self) -> None:
        test_flake_to_timeout = (
            "spec@!opengl 1.1@depthstencil-default_fb-drawpixels-24_8 samples=2"
        )
        test_fail_to_timeout = "spec@!opengl 1.1@linestipple"

        self.expectation_changes[ExpectationCategories.TIMEOUT] = {
            test_flake_to_timeout,
            test_fail_to_timeout,
        }

        self.assert_expectation_present(test_flake_to_timeout, self.flakes_expectations)
        self.assert_expectation_present(test_fail_to_timeout, self.fails_expectations)

        processor = self.create_expectations_processor()
        processor.process_expectation_changes()

        self.assert_expectation_missing(test_flake_to_timeout, self.flakes_expectations)
        self.assert_expectation_missing(test_fail_to_timeout, self.fails_expectations)
        self.assert_expectation_present(test_flake_to_timeout, self.skips_expectations)
        self.assert_expectation_present(test_fail_to_timeout, self.skips_expectations)

    def test_new_fail(self) -> None:
        new_fail = "foo bar"

        self.expectation_changes[ExpectationCategories.FAIL] = {new_fail}

        self.assert_expectation_missing(new_fail, self.fails_expectations)

        processor = self.create_expectations_processor()
        processor.process_expectation_changes(True)

        fail_line = self.assert_expectation_present(new_fail, self.fails_expectations)
        self.assertTrue(fail_line.endswith(",Fail\n"))

    def test_new_crash(self) -> None:
        new_crash = "foo bar"

        self.expectation_changes[ExpectationCategories.CRASH] = {new_crash}

        self.assert_expectation_missing(new_crash, self.fails_expectations)

        processor = self.create_expectations_processor()
        processor.process_expectation_changes(True)

        fail_line = self.assert_expectation_present(new_crash, self.fails_expectations)
        self.assertTrue(fail_line.endswith(",Crash\n"))

    def test_fail_to_crash(self) -> None:
        test_name_to_crash = "spec@arb_es2_compatibility@texwrap formats"

        self.expectation_changes[ExpectationCategories.CRASH] = {test_name_to_crash}

        crash_line = self.assert_expectation_present(
            test_name_to_crash, self.fails_expectations
        )
        self.assertTrue(crash_line.endswith(",Fail\n"))

        processor = self.create_expectations_processor()
        processor.process_expectation_changes()

        # if record of the fail is not removed this call will return this old record, not the inserted
        fail_line = self.assert_expectation_present(
            test_name_to_crash, self.fails_expectations
        )
        self.assertTrue(fail_line.endswith(",Crash\n"))

    def test_fail_to_skip(self) -> None:
        test_name_to_skip = (
            "spec@!opengl 1.1@polygon-mode-offset@config 5: "
            "Expected white pixel on top edge"
        )

        self.expectation_changes[ExpectationCategories.UNEXPECTED_IMPROVEMENT_SKIP] = {
            test_name_to_skip
        }

        self.assert_expectation_present(test_name_to_skip, self.fails_expectations)

        processor = self.create_expectations_processor()
        processor.process_expectation_changes()

        self.assert_expectation_missing(test_name_to_skip, self.fails_expectations)
        self.assert_expectation_missing(test_name_to_skip, self.skips_expectations)

    def test_fail_to_warn(self) -> None:
        test_name_to_warn = (
            "dEQP-GLES2.functional.texture.filtering.2d."
            "nearest_mipmap_nearest_linear_mirror_rgba8888"
        )

        self.expectation_changes[ExpectationCategories.UNEXPECTED_IMPROVEMENT_WARN] = {
            test_name_to_warn
        }

        self.assert_expectation_present(test_name_to_warn, self.fails_expectations)

        processor = self.create_expectations_processor()
        processor.process_expectation_changes()

        self.assert_expectation_missing(test_name_to_warn, self.fails_expectations)
        self.assert_expectation_missing(test_name_to_warn, self.flakes_expectations)
        self.assert_expectation_missing(test_name_to_warn, self.skips_expectations)

    def test_crash_to_fail(self) -> None:
        test_name_to_fail = (
            "spec@arb_draw_elements_base_vertex@"
            "arb_draw_elements_base_vertex-negative-index"
        )

        self.expectation_changes[ExpectationCategories.UNEXPECTED_IMPROVEMENT_FAIL] = {
            test_name_to_fail
        }

        crash_line = self.assert_expectation_present(
            test_name_to_fail, self.fails_expectations
        )
        self.assertTrue(crash_line.endswith(",Crash\n"))

        processor = self.create_expectations_processor()
        processor.process_expectation_changes()

        fail_line = self.assert_expectation_present(
            test_name_to_fail, self.fails_expectations
        )
        self.assertTrue(fail_line.endswith(",Fail\n"))

    def test_add_record_fails_on_sorted_block(self) -> None:
        # problem reproduced from https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/33547
        #  From nightly run pipeline https://gitlab.freedesktop.org/mesa/mesa/-/pipelines/1364513
        #  ci-collate produced a patch with many registers in a *-fails.txt file without the test state.
        #  One sample, among others, happened with the job "v3dv-rpi5-vk-full:arm64"
        # 4 tests failed and 1 crashed:
        #  The failing ones create a new block in the *-fails.txt file, as a new block, the tests are sorted.
        #  The crashing one is added to the new block (sorted), then the insertion doesn't include the test_result.
        # There is a racing condition as there are two keys in self.expectation_changes
        #  The first category processed produces the new block, the second inserts in the block
        tests_to_fail = {
            "dEQP-VK.binding_model.shader_access.secondary_cmd_buf.bind.with_template.storage_image.geometry.multiple_descriptor_sets.descriptor_array.3d_base_mip",
            "dEQP-VK.subgroups.size_control.compute.require_full_subgroups_spirv16",
            "dEQP-VK.texture.explicit_lod.2d.sizes.128x128_linear_linear_mipmap_linear_clamp",
            "dEQP-VK.texture.shadow.2d_array.linear_mipmap_nearest.greater_x8_d24_unorm_pack32",
        }
        tests_to_crash = {
            "dEQP-VK.memory_model.write_after_read.ext.u32.noncoherent.fence_fence.atomicwrite.subgroup.payload_local.image.guard_local.image.frag"
        }
        self.expectation_changes[ExpectationCategories.FAIL] = tests_to_fail
        self.expectation_changes[ExpectationCategories.CRASH] = tests_to_crash

        for tests_set in [tests_to_fail, tests_to_crash]:
            for test_name in tests_set:
                for expectation_file in [
                    self.fails_expectations,
                    self.flakes_expectations,
                    self.skips_expectations,
                ]:
                    self.assert_expectation_missing(test_name, expectation_file)

        processor = self.create_expectations_processor()

        self.assertEqual(self.fails_expectations.n_blocks, 31)

        processor.process_expectation_changes()

        # there is a new block with the new records.
        self.assertEqual(self.fails_expectations.n_blocks, 32)

        # Due to a race condition, the bug can produce fails or crash without the suffix
        #  Both need to be verified. Also because we want to reproduce exactly the output expected.
        for test_name in tests_to_fail:
            fail_line = self.assert_expectation_present(
                test_name, self.fails_expectations
            )
            self.assertTrue(fail_line.endswith(",Fail\n"))
        for test_name in tests_to_crash:
            fail_line = self.assert_expectation_present(
                test_name, self.fails_expectations
            )
            self.assertTrue(fail_line.endswith(",Crash\n"))

    def test_unmanaged(self) -> None:
        unmanaged_sample: dict[ExpectationCategories, set[str]] = {
            ExpectationCategories.SKIP: {"foo2", "bar2"},
            ExpectationCategories.WARN: {"foo", "bar"},
            ExpectationCategories.EXPECTED_FAIL: {"foo1", "bar1"},
            ExpectationCategories.KNOWN_FLAKE: {"foo3", "bar3"},
        }

        self.expectation_changes.update(unmanaged_sample)

        with self.assertLogs(logger, "DEBUG") as logs_record:
            processor = self.create_expectations_processor()
            processor.process_expectation_changes()

        for unmanaged_category in unmanaged_sample.keys():
            self.assertTrue(
                any(unmanaged_category in log for log in logs_record.output)
            )
