# Copyright (C) 2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

from pathlib import Path
from tempfile import TemporaryDirectory
from unittest import TestCase
from zlib import crc32

import responses

from glcollate import Collate

TEST_ROOT = Path(__file__).parent
RECORD_PIPELINE_ARTIFACTS_FILE = "test_pipeline_artifacts.yaml"
RECORD_PIPELINE_REFRESH_FILE = "test_pipeline_refresh_{sequence}.yaml"


class TestPipeline(TestCase):
    def test_pipeline_artifacts(self) -> None:
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(str(TEST_ROOT / RECORD_PIPELINE_ARTIFACTS_FILE))

            collate = Collate(
                gitlab_url="https://gitlab.freedesktop.org",
                gitlab_token_file_name="/dev/null",
                namespace="mesa",
                project="mesa",
            )
            pipeline = collate.from_pipeline(1310796)
            artifacts_dict = pipeline.get_artifact(
                "results/failures.csv",
                job_regex=None,
                status="failed",
                stage=None,
            )

        artifacts_hashes = {
            nk: {ik: crc32(iv.content_as_bytes) for ik, iv in nv.items()}
            for nk, nv in artifacts_dict.items()
        }

        self.assertEqual(
            artifacts_hashes,
            {
                "panfrost-g610-vk:arm64 1/5": {66635589: 2163483772},
                "panfrost-g610-vk:arm64 2/5": {66635590: 364460798},
                "panfrost-g610-vk:arm64 3/5": {66635591: 810765406},
                "panfrost-g610-vk:arm64 4/5": {66635592: 3072972251},
                "panfrost-g610-vk:arm64 5/5": {66635593: 557582576},
            },
        )

    def test_pipeline_patch(self) -> None:
        with TemporaryDirectory() as temp_dir_name, responses.RequestsMock() as rsps:
            patch_file = Path(temp_dir_name) / "test"

            rsps._add_from_file(str(TEST_ROOT / "test_pipeline_patch.yaml"))

            collate = Collate(
                gitlab_url="https://gitlab.freedesktop.org",
                gitlab_token_file_name="/dev/null",
                namespace="mesa",
                project="mesa",
            )
            pipeline = collate.from_pipeline(1322188)
            pipeline.expectations_update(
                branch_namespace="mesa",
                patch_branch="mesa-24.3.1",
                jobs=[".*radv-vangogh.*"],
                patch_name=str(patch_file),
            )

            with open(f"{patch_file}.patch") as f:
                self.assertGreater(len(f.read()), 0)

    def test_pipeline_refresh(self) -> None:
        pipeline_id = 1329142

        # pipeline's starting point
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(
                str(TEST_ROOT / RECORD_PIPELINE_REFRESH_FILE.format(sequence="0"))
            )
            collate = Collate(
                gitlab_url="https://gitlab.freedesktop.org",
                gitlab_token_file_name="/dev/null",
                namespace="virgl",
                project="virglrenderer",
            )
            pipeline = collate.from_pipeline(pipeline_id)
            self.assertEqual(pipeline.status, "manual")
            jobs_stata = pipeline.job_stata
            self.assertEqual(jobs_stata, {"created", "manual"})
            jobs_by_status = {
                status: pipeline.jobs_in_status(status) for status in jobs_stata
            }
            self.assertEqual(len(jobs_by_status["created"]), 14)
            self.assertEqual(len(jobs_by_status["manual"]), 9)
            stages = pipeline.pipeline_stages
            self.assertEqual(stages, {"build", "sanity test", "test"})
            jobs_by_stage = {stage: pipeline.jobs_in_stage(stage) for stage in stages}
            self.assertEqual(len(jobs_by_stage["build"]), 5)
            self.assertEqual(len(jobs_by_stage["sanity test"]), 4)
            self.assertEqual(len(jobs_by_stage["test"]), 14)

        # refresh when jobs where triggered
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(
                str(TEST_ROOT / RECORD_PIPELINE_REFRESH_FILE.format(sequence="1"))
            )
            pipeline.refresh()
            self.assertEqual(pipeline.status, "running")
            self.assertEqual(pipeline.previous_status, "manual")
            self.assertTrue(pipeline.status_changed)
            jobs_stata = pipeline.job_stata
            self.assertEqual(jobs_stata, {"created", "pending", "running"})
            jobs_by_status = {
                status: pipeline.jobs_in_status(status) for status in jobs_stata
            }
            self.assertEqual(len(jobs_by_status["created"]), 14)
            self.assertEqual(len(jobs_by_status["pending"]), 1)
            self.assertEqual(len(jobs_by_status["running"]), 8)

        # refresh when test stage starts having jobs pending/running
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(
                str(TEST_ROOT / RECORD_PIPELINE_REFRESH_FILE.format(sequence="2"))
            )
            pipeline.refresh()
            self.assertEqual(pipeline.status, "running")
            self.assertIsNone(pipeline.previous_status)
            self.assertFalse(pipeline.status_changed)
            jobs_stata = pipeline.job_stata
            self.assertEqual(jobs_stata, {"running", "success"})
            jobs_by_status = {
                status: pipeline.jobs_in_status(status) for status in jobs_stata
            }
            self.assertEqual(len(jobs_by_status["running"]), 15)
            self.assertEqual(len(jobs_by_status["success"]), 8)

        # refresh when one job in test stage completes
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(
                str(TEST_ROOT / RECORD_PIPELINE_REFRESH_FILE.format(sequence="3"))
            )
            pipeline.refresh()
            self.assertEqual(pipeline.status, "running")
            self.assertIsNone(pipeline.previous_status)
            self.assertFalse(pipeline.status_changed)
            jobs_stata = pipeline.job_stata
            self.assertEqual(jobs_stata, {"running", "success"})
            jobs_by_status = {
                status: pipeline.jobs_in_status(status) for status in jobs_stata
            }
            self.assertEqual(len(jobs_by_status["running"]), 13)
            self.assertEqual(len(jobs_by_status["success"]), 10)

        # retry one test job
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(
                str(TEST_ROOT / RECORD_PIPELINE_REFRESH_FILE.format(sequence="4"))
            )
            pipeline.refresh()
            self.assertEqual(pipeline.status, "running")
            self.assertIsNone(pipeline.previous_status)
            self.assertFalse(pipeline.status_changed)
            jobs_stata = pipeline.job_stata
            self.assertEqual(jobs_stata, {"running", "success"})
            jobs_by_status = {
                status: pipeline.jobs_in_status(status) for status in jobs_stata
            }
            self.assertEqual(len(jobs_by_status["running"]), 14)
            self.assertEqual(len(jobs_by_status["success"]), 9)

        # refresh when pipeline finish
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(
                str(TEST_ROOT / RECORD_PIPELINE_REFRESH_FILE.format(sequence="5"))
            )
            pipeline.refresh()
            self.assertEqual(pipeline.status, "success")
            self.assertEqual(pipeline.previous_status, "running")
            self.assertTrue(pipeline.status_changed)
            jobs_stata = pipeline.job_stata
            self.assertEqual(jobs_stata, {"success"})
            jobs_by_status = {
                status: pipeline.jobs_in_status(status) for status in jobs_stata
            }
            self.assertEqual(len(jobs_by_status["success"]), 23)


# TODO: test a pipeline refresh and its changes
#  - change job filter between refresh
